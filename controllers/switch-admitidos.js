
/* Estos selectores sirven para las consultas genéricas. Ejemplo: "celulares samsung". El predictor 
 * de ML devuelve categorías como "otros modelos", "otras clases", etc. En presencia de esos casos, se 
 * admiten categorías que no sean iguales al momento de compararlas y se incluye el producto en el 
 * listado que se muestra. 
 * 
 * Para mayor legilibilidad y para acortar el scope, se dividen los switch por categorías. */

module.exports.switchGeneral = function(idCategoria,callback){
	switch(idCategoria) {
	//Celulares y Smartphones
	case "MLA1051":
		callback(switchCelularesYSmartphones);
		break;
	//Electrodomésticos y Aires.
	case "MLA5726":
		callback(switchElectrodomesticos);
		break;
	//Electrónica, Audio y Video.
	case "MLA1000":
		callback(switchElectronicaAudioVideo);
		break;
	//Computación
	case "MLA1648":
		callback(switchComputacion);
		break;
	}
};

function switchComputacion(idML,callback){

	var admite = false;

	switch(idML) {
	
	}
	
	callback(admite);
}

function switchElectronicaAudioVideo(idML,callback){

	var admite = false;

	switch(idML) {

	//Auriculares.
		//Clip de Oreja.
	case "MLA44389":
		admite = true;
		break;
		//Con Vincha
	case "MLA10585":
		admite = true;
		break;
		//In Ear
	case "MLA10558":
		admite = true;
		break;
		
	//Televisores.
		//Smart TV.
	case "MLA123858": 
		admite = true;
		break;
		//LED
	case "MLA81531": 
		admite = true;
		break;
		//4k
	case "MLA321526": 
		admite = true;
		break;
		
		
	//Inicio Electrónica Audio y Video.
	case "MLA1070":
		admite = true;
		break;
	case "MLA410300":
		admite = true;
		break;
	case "MLA5041":
		admite = true;
		break;
	case "MLA3698":
		admite = true;
		break;
	case "MLA12812":
		admite = true;
		break;
	case "MLA2916":
		admite = true;
		break;
	case "MLA409811":
		admite = true;
		break;
	case "MLA403001":
		admite = true;
		break;
	case "MLA1064":
		admite = true;
		break;
	case "MLA10722":
		admite = true;
		break;
	case "MLA4799":
		admite = true;
		break;
	case "MLA4640":
		admite = true;
		break;
	case "MLA2831":
		admite = true;
		break;
	case "MLA11822":
		admite = true;
		break;
	case "MLA73553":
		admite = true;
		break;
	case "MLA10567":
		admite = true;
		break;
	case "MLA352002":
		admite = true;
		break;
	case "MLA49342":
		admite = true;
		break;
	case "MLA409561":
		admite = true;
		break;
	case "MLA44384":
		admite = true;
		break;
	case "MLA91787":
		admite = true;
		break;
	case "MLA409591":
		admite = true;
		break;
	case "MLA2860":
		admite = true;
		break;
	case "MLA74670":
		admite = true;
		break;
	case "MLA1916":
		admite = true;
		break;
	case "MLA1913":
		admite = true;
		break;
	case "MLA92636":
		admite = true;
		break;
	case "MLA92215":
		admite = true;
		break;
	case "MLA10573":
		admite = true;
		break;
	case "MLA411082":
		admite = true;
		break;
	case "MLA125417":
		admite = true;
		break;
	case "MLA410681":
		admite = true;
		break;
	case "MLA49554":
		admite = true;
		break;
	case "MLA7570":
		admite = true;
		break;
	case "MLA91690":
		admite = true;
		break;
	case "MLA49359":
		admite = true;
		break;
	case "MLA389370":
		admite = true;
		break;
	case "MLA36559":
		admite = true;
		break;
	case "MLA36587":
		admite = true;
		break;
	case "MLA389369":
		admite = true;
		break;
	case "MLA389362":
		admite = true;
		break;
	case "MLA60353":
		admite = true;
		break;
	case "MLA8790":
		admite = true;
		break;
	case "MLA30816":
		admite = true;
		break;
	case "MLA94880":
		admite = true;
		break;
	case "MLA2879":
		admite = true;
		break;
	case "MLA60368":
		admite = true;
		break;
	case "MLA91725":
		admite = true;
		break;
	case "MLA389367":
		admite = true;
		break;
	case "MLA5047":
		admite = true;
		break;
	case "MLA60282":
		admite = true;
		break;
	case "MLA323048":
		admite = true;
		break;
	case "MLA91866":
		admite = true;
		break;
	case "MLA49305":
		admite = true;
		break;
	case "MLA322877":
		admite = true;
		break;
	case "MLA4637":
		admite = true;
		break;
	case "MLA10474":
		admite = true;
		break;
	case "MLA389593":
		admite = true;
		break;
	case "MLA391304":
		admite = true;
		break;
	case "MLA390207":
		admite = true;
		break;
	case "MLA324035":
		admite = true;
		break;
	case "MLA410324":
		admite = true;
		break;
	case "MLA391580":
		admite = true;
		break;
	case "MLA410434":
		admite = true;
		break;
	case "MLA390606":
		admite = true;
		break;
	case "MLA2866":
		admite = true;
		break;
	case "MLA92223":
		admite = true;
		break;
	case "MLA323337":
		admite = true;
		break;
	case "MLA5984":
		admite = true;
		break;
	case "MLA391536":
		admite = true;
		break;
	case "MLA10572":
		admite = true;
		break;
	case "MLA390321":
		admite = true;
		break;
	case "MLA389439":
		admite = true;
		break;
	case "MLA125419":
		admite = true;
		break;
	case "MLA60305":
		admite = true;
		break;
	case "MLA60281":
		admite = true;
		break;
	case "MLA390359":
		admite = true;
		break;
	case "MLA391458":
		admite = true;
		break;
	case "MLA49388":
		admite = true;
		break;
	case "MLA8723":
		admite = true;
		break;
	case "MLA323997":
		admite = true;
		break;
	case "MLA90686":
		admite = true;
		break;
	case "MLA4254":
		admite = true;
		break;
	case "MLA389368":
		admite = true;
		break;
	case "MLA90722":
		admite = true;
		break;
	case "MLA410508":
		admite = true;
		break;
	case "MLA125052":
		admite = true;
		break;
	case "MLA389931":
		admite = true;
		break;
	case "MLA389706":
		admite = true;
		break;
	case "MLA391370":
		admite = true;
		break;
	case "MLA389365":
		admite = true;
		break;
	case "MLA389819":
		admite = true;
		break;
	case "MLA410368":
		admite = true;
		break;
	case "MLA125418":
		admite = true;
		break;
	case "MLA389458":
		admite = true;
		break;
	case "MLA401663":
		admite = true;
		break;
	case "MLA10472":
		admite = true;
		break;
	case "MLA10581":
		admite = true;
		break;
	case "MLA390226":
		admite = true;
		break;
	case "MLA391414":
		admite = true;
		break;
	case "MLA125421":
		admite = true;
		break;
	case "MLA410618":
		admite = true;
		break;
	case "MLA391326":
		admite = true;
		break;
	case "MLA73513":
		admite = true;
		break;
	case "MLA411149":
		admite = true;
		break;
	case "MLA391624":
		admite = true;
		break;
	case "MLA323010":
		admite = true;
		break;
	case "MLA389603":
		admite = true;
		break;
	case "MLA92340":
		admite = true;
		break;
	case "MLA389515":
		admite = true;
		break;
	case "MLA389366":
		admite = true;
		break;
	case "MLA91780":
		admite = true;
		break;
	case "MLA324073":
		admite = true;
		break;
	case "MLA390397":
		admite = true;
		break;
	case "MLA87025":
		admite = true;
		break;
	case "MLA410552":
		admite = true;
		break;
	case "MLA324301":
		admite = true;
		break;
	case "MLA60309":
		admite = true;
		break;
	case "MLA73519":
		admite = true;
		break;
	case "MLA391251":
		admite = true;
		break;
	case "MLA389893":
		admite = true;
		break;
	case "MLA410574":
		admite = true;
		break;
	case "MLA389574":
		admite = true;
		break;
	case "MLA389829":
		admite = true;
		break;
	case "MLA390435":
		admite = true;
		break;
	case "MLA391097":
		admite = true;
		break;
	case "MLA60395":
		admite = true;
		break;
	case "MLA405188":
		admite = true;
		break;
	case "MLA389364":
		admite = true;
		break;
	case "MLA70025":
		admite = true;
		break;
	case "MLA322991":
		admite = true;
		break;
	case "MLA8805":
		admite = true;
		break;
	case "MLA90708":
		admite = true;
		break;
	case "MLA390062":
		admite = true;
		break;
	case "MLA90688":
		admite = true;
		break;
	case "MLA390245":
		admite = true;
		break;
	case "MLA49364":
		admite = true;
		break;
	case "MLA390169":
		admite = true;
		break;
	case "MLA390264":
		admite = true;
		break;
	case "MLA391558":
		admite = true;
		break;
	case "MLA390416":
		admite = true;
		break;
	case "MLA391119":
		admite = true;
		break;
	case "MLA369275":
		admite = true;
		break;
	case "MLA125420":
		admite = true;
		break;
	case "MLA389555":
		admite = true;
		break;
	case "MLA91798":
		admite = true;
		break;
	case "MLA73510":
		admite = true;
		break;
	case "MLA390530":
		admite = true;
		break;
	case "MLA391207":
		admite = true;
		break;
	case "MLA323356":
		admite = true;
		break;
	case "MLA391436":
		admite = true;
		break;
	case "MLA90690":
		admite = true;
		break;
	case "MLA90725":
		admite = true;
		break;
	case "MLA410346":
		admite = true;
		break;
	case "MLA390473":
		admite = true;
		break;
	case "MLA85727":
		admite = true;
		break;
	case "MLA32229":
		admite = true;
		break;
	case "MLA125005":
		admite = true;
		break;
	case "MLA58770":
		admite = true;
		break;
	case "MLA125423":
		admite = true;
		break;
	case "MLA323280":
		admite = true;
		break;
	case "MLA391163":
		admite = true;
		break;
	case "MLA323569":
		admite = true;
		break;
	case "MLA60351":
		admite = true;
		break;
	case "MLA391668":
		admite = true;
		break;
	case "MLA389525":
		admite = true;
		break;
	case "MLA389941":
		admite = true;
		break;
	case "MLA8717":
		admite = true;
		break;
	case "MLA403647":
		admite = true;
		break;
	case "MLA391392":
		admite = true;
		break;
	case "MLA410662":
		admite = true;
		break;
	case "MLA389496":
		admite = true;
		break;
	case "MLA74673":
		admite = true;
		break;
	case "MLA391602":
		admite = true;
		break;
	case "MLA391273":
		admite = true;
		break;
	case "MLA60380":
		admite = true;
		break;
	case "MLA323029":
		admite = true;
		break;
	case "MLA125426":
		admite = true;
		break;
	case "MLA322896":
		admite = true;
		break;
	case "MLA389650":
		admite = true;
		break;
	case "MLA324225":
		admite = true;
		break;
	case "MLA321632":
		admite = true;
		break;
	case "MLA410530":
		admite = true;
		break;
	case "MLA390587":
		admite = true;
		break;
	case "MLA410478":
		admite = true;
		break;
	case "MLA2863":
		admite = true;
		break;
	case "MLA88435":
		admite = true;
		break;
	case "MLA323299":
		admite = true;
		break;
	case "MLA390549":
		admite = true;
		break;
	case "MLA389781":
		admite = true;
		break;
	case "MLA352010":
		admite = true;
		break;
	case "MLA324187":
		admite = true;
		break;
	case "MLA90700":
		admite = true;
		break;
	case "MLA389406":
		admite = true;
		break;
	case "MLA91790":
		admite = true;
		break;
	case "MLA60160":
		admite = true;
		break;
	case "MLA73537":
		admite = true;
		break;
	case "MLA389416":
		admite = true;
		break;
	case "MLA2870":
		admite = true;
		break;
	case "MLA125425":
		admite = true;
		break;
	case "MLA407802":
		admite = true;
		break;
	case "MLA390302":
		admite = true;
		break;
	case "MLA321536":
		admite = true;
		break;
	case "MLA36475":
		admite = true;
		break;
	case "MLA390131":
		admite = true;
		break;
	case "MLA323413":
		admite = true;
		break;
	case "MLA125042":
		admite = true;
		break;
	case "MLA10481":
		admite = true;
		break;
	case "MLA389754":
		admite = true;
		break;
	case "MLA410390":
		admite = true;
		break;
	case "MLA125015":
		admite = true;
		break;
	case "MLA390091":
		admite = true;
		break;
	case "MLA324111":
		admite = true;
		break;
	case "MLA391690":
		admite = true;
		break;
	case "MLA70044":
		admite = true;
		break;
	case "MLA323261":
		admite = true;
		break;
	case "MLA49369":
		admite = true;
		break;
	case "MLA391503":
		admite = true;
		break;
	case "MLA32239":
		admite = true;
		break;
	case "MLA391185":
		admite = true;
		break;
	case "MLA125025":
		admite = true;
		break;
	case "MLA410596":
		admite = true;
		break;
	case "MLA391646":
		admite = true;
		break;
	case "MLA390378":
		admite = true;
		break;
	case "MLA91856":
		admite = true;
		break;
	case "MLA49324":
		admite = true;
		break;
	case "MLA60285":
		admite = true;
		break;
	case "MLA352035":
		admite = true;
		break;
	case "MLA44537":
		admite = true;
		break;
	case "MLA389631":
		admite = true;
		break;
	case "MLA410412":
		admite = true;
		break;
	case "MLA60291":
		admite = true;
		break;
	case "MLA91751":
		admite = true;
		break;
	case "MLA352075":
		admite = true;
		break;
	case "MLA324149":
		admite = true;
		break;
	case "MLA405986":
		admite = true;
		break;
	case "MLA125422":
		admite = true;
		break;
	case "MLA12792":
		admite = true;
		break;
	case "MLA391480":
		admite = true;
		break;
	case "MLA323318":
		admite = true;
		break;
	case "MLA323432":
		admite = true;
		break;
	case "MLA91844":
		admite = true;
		break;
	case "MLA390283":
		admite = true;
		break;
	case "MLA5061":
		admite = true;
		break;
	case "MLA91711":
		admite = true;
		break;
	case "MLA390188":
		admite = true;
		break;
	case "MLA390568":
		admite = true;
		break;
	case "MLA70049":
		admite = true;
		break;
	case "MLA91718":
		admite = true;
		break;
	case "MLA32217":
		admite = true;
		break;
	case "MLA60292":
		admite = true;
		break;
	case "MLA18186":
		admite = true;
		break;
	case "MLA410456":
		admite = true;
		break;
	case "MLA389912":
		admite = true;
		break;
	case "MLA60341":
		admite = true;
		break;
	case "MLA389669":
		admite = true;
		break;
	case "MLA389679":
		admite = true;
		break;
	case "MLA5044":
		admite = true;
		break;
	case "MLA390511":
		admite = true;
		break;
	case "MLA389477":
		admite = true;
		break;
	case "MLA389800":
		admite = true;
		break;
	case "MLA91704":
		admite = true;
		break;
	case "MLA323588":
		admite = true;
		break;
	case "MLA125424":
		admite = true;
		break;
	case "MLA321256":
		admite = true;
		break;
	case "MLA91705":
		admite = true;
		break;
	case "MLA389744":
		admite = true;
		break;
	case "MLA389725":
		admite = true;
		break;
	case "MLA390081":
		admite = true;
		break;
	case "MLA410640":
		admite = true;
		break;
	case "MLA352055":
		admite = true;
		break;
	case "MLA8009":
		admite = true;
		break;
	case "MLA390043":
		admite = true;
		break;
	case "MLA352065":
		admite = true;
		break;
	case "MLA391229":
		admite = true;
		break;
	case "MLA7454":
		admite = true;
		break;
	case "MLA390340":
		admite = true;
		break;
	case "MLA352045":
		admite = true;
		break;
	case "MLA125061":
		admite = true;
		break;
	case "MLA390454":
		admite = true;
		break;
	case "MLA390492":
		admite = true;
		break;
	}
	callback(admite);
}

function switchElectrodomesticos(idML,callback){

	var admite = false;

	switch(idML) {

	//Lavarropas Automático o Semi automático.
	case "MLA10077":
		admite = true;
		break;
	case "MLA10062":
		admite = true;
		break;

		//Lavarropas: carga superior o frontal (automático y semi automático).
	case "MLA10282":
		admite = true;
		break;
	case "MLA10287":
		admite = true;
		break;
	case "MLA18375":
		admite = true;
		break;
	case "MLA18376":
		admite = true;
		break;

		//Inicio Electrodomésticos y Aires
	case "MLA5977":
		admite = true;
		break;
	case "MLA1899":
		admite = true;
		break;
	case "MLA6356":
		admite = true;
		break;
	case "MLA21087":
		admite = true;
		break;
	case "MLA24375":
		admite = true;
		break;
	case "MLA117260":
		admite = true;
		break;
	case "MLA1620":
		admite = true;
		break;
	case "MLA385178":
		admite = true;
		break;
	case "MLA392351":
		admite = true;
		break;
	case "MLA9461":
		admite = true;
		break;
	case "MLA74267":
		admite = true;
		break;
	case "MLA74248":
		admite = true;
		break;
	case "MLA398699":
		admite = true;
		break;
	case "MLA29876":
		admite = true;
		break;
	case "MLA125479":
		admite = true;
		break;
	case "MLA126114":
		admite = true;
		break;
	case "MLA125764":
		admite = true;
		break;
	case "MLA74268":
		admite = true;
		break;
	case "MLA401945":
		admite = true;
		break;
	case "MLA404096":
		admite = true;
		break;
	case "MLA126160":
		admite = true;
		break;
	case "MLA407210":
		admite = true;
		break;
	case "MLA126124":
		admite = true;
		break;
	case "MLA74282":
		admite = true;
		break;
	case "MLA125434":
		admite = true;
		break;
	case "MLA392340":
		admite = true;
		break;
	case "MLA126056":
		admite = true;
		break;
	case "MLA125718":
		admite = true;
		break;
	case "MLA126302":
		admite = true;
		break;
	case "MLA385503":
		admite = true;
		break;
	case "MLA398571":
		admite = true;
		break;
	case "MLA9338":
		admite = true;
		break;
	case "MLA393750":
		admite = true;
		break;
	case "MLA126210":
		admite = true;
		break;
	case "MLA125552":
		admite = true;
		break;
	case "MLA125637":
		admite = true;
		break;
	case "MLA1647":
		admite = true;
		break;
	case "MLA125865":
		admite = true;
		break;
	case "MLA125474":
		admite = true;
		break;
	case "MLA125684":
		admite = true;
		break;
	case "MLA10098":
		admite = true;
		break;
	case "MLA74330":
		admite = true;
		break;
	case "MLA125789":
		admite = true;
		break;
	case "MLA125476":
		admite = true;
		break;
	case "MLA125493":
		admite = true;
		break;
	case "MLA18407":
		admite = true;
		break;
	case "MLA125475":
		admite = true;
		break;
	case "MLA410950":
		admite = true;
		break;
	case "MLA74303":
		admite = true;
		break;
	case "MLA126038":
		admite = true;
		break;
	case "MLA385690":
		admite = true;
		break;
	case "MLA126058":
		admite = true;
		break;
	case "MLA125855":
		admite = true;
		break;
	case "MLA389255":
		admite = true;
		break;
	case "MLA126346":
		admite = true;
		break;
	case "MLA392409":
		admite = true;
		break;
	case "MLA126078":
		admite = true;
		break;
	case "MLA321251":
		admite = true;
		break;
	case "MLA401617":
		admite = true;
		break;
	case "MLA122493":
		admite = true;
		break;
	case "MLA126068":
		admite = true;
		break;
	case "MLA116430":
		admite = true;
		break;
	case "MLA398583":
		admite = true;
		break;
	case "MLA126168":
		admite = true;
		break;
	case "MLA392016":
		admite = true;
		break;
	case "MLA321826":
		admite = true;
		break;
	case "MLA126088":
		admite = true;
		break;
	case "MLA125692":
		admite = true;
		break;
	case "MLA126238":
		admite = true;
		break;
	case "MLA385701":
		admite = true;
		break;
	case "MLA126211":
		admite = true;
		break;
	case "MLA74311":
		admite = true;
		break;
	case "MLA401145":
		admite = true;
		break;
	case "MLA399250":
		admite = true;
		break;
	case "MLA125500":
		admite = true;
		break;
	case "MLA10073":
		admite = true;
		break;
	case "MLA125875":
		admite = true;
		break;
	case "MLA126356":
		admite = true;
		break;
	case "MLA119702":
		admite = true;
		break;
	case "MLA125976":
		admite = true;
		break;
	case "MLA126188":
		admite = true;
		break;
	case "MLA126220":
		admite = true;
		break;
	case "MLA119729":
		admite = true;
		break;
	case "MLA122496":
		admite = true;
		break;
	case "MLA122495":
		admite = true;
		break;
	case "MLA125486":
		admite = true;
		break;
	case "MLA125845":
		admite = true;
		break;
	case "MLA321813":
		admite = true;
		break;
	case "MLA74264":
		admite = true;
		break;
	case "MLA126178":
		admite = true;
		break;
	case "MLA126009":
		admite = true;
		break;
	case "MLA43688":
		admite = true;
		break;
	case "MLA321252":
		admite = true;
		break;
	case "MLA404101":
		admite = true;
		break;
	case "MLA126229":
		admite = true;
		break;
	case "MLA125700":
		admite = true;
		break;
	case "MLA126198":
		admite = true;
		break;
	case "MLA119720":
		admite = true;
		break;
	case "MLA10237":
		admite = true;
		break;
	case "MLA74265":
		admite = true;
		break;
	case "MLA74275":
		admite = true;
		break;
	case "MLA392356":
		admite = true;
		break;
	case "MLA125776":
		admite = true;
		break;
	case "MLA401601":
		admite = true;
		break;
	case "MLA401476":
		admite = true;
		break;
	case "MLA398615":
		admite = true;
		break;
	case "MLA122494":
		admite = true;
		break;
	case "MLA401154":
		admite = true;
		break;
	case "MLA125965":
		admite = true;
		break;
	case "MLA126042":
		admite = true;
		break;
	case "MLA126351":
		admite = true;
		break;
	case "MLA125987":
		admite = true;
		break;
	case "MLA125731":
		admite = true;
		break;
	case "MLA29882":
		admite = true;
		break;
	case "MLA74315":
		admite = true;
		break;
	case "MLA126040":
		admite = true;
		break;
	case "MLA125998":
		admite = true;
		break;
	case "MLA126036":
		admite = true;
		break;
	case "MLA119711":
		admite = true;
		break;
	case "MLA126020":
		admite = true;
		break;
	case "MLA125802":
		admite = true;
		break;
	case "MLA321783":
		admite = true;
		break;
	case "MLA410965":
		admite = true;
		break;
	case "MLA321787":
		admite = true;
		break;
	case "MLA321800":
		admite = true;
		break;
	case "MLA74253":
		admite = true;
		break;

	}

	callback(admite);	
}

function switchCelularesYSmartphones(idML,callback){

	var admite = false;

	switch(idML) {

		//Inicio Celulares y Smartphones
	case "MLA1915":
		admite = true;
		break;
	case "MLA3529":
		admite = true;
		break;
	case "MLA2902":
		admite = true;
		break;
	case "MLA3526":
		admite = true;
		break;
	case "MLA17663":
		admite = true;
		break;
	case "MLA12999":
		admite = true;
		break;
	case "MLA5549":
		admite = true;
		break;
	case "MLA2899":
		admite = true;
		break;
	case "MLA10676":
		admite = true;
		break;
	case "MLA2911":
		admite = true;
		break;
	case "MLA11373":
		admite = true;
		break;
	case "MLA53603":
		admite = true;
		break;
	case "MLA73035":
		admite = true;
		break;
	case "MLA5340":
		admite = true;
		break;
	case "MLA6424":
		admite = true;
		break;
	case "MLA10630":
		admite = true;
		break;
	case "MLA30136":
		admite = true;
		break;
	case "MLA393389":
		admite = true;
		break;
	case "MLA13004":
		admite = true;
		break;
	case "MLA340371":
		admite = true;
		break;
	case "MLA386059":
		admite = true;
		break;
	case "MLA409343":
		admite = true;
		break;
	case "MLA393555":
		admite = true;
		break;
	case "MLA10219":
		admite = true;
		break;
	case "MLA4172":
		admite = true;
		break;
	case "MLA58634":
		admite = true;
		break;
	case "MLA86903":
		admite = true;
		break;
	case "MLA12994":
		admite = true;
		break;
	case "MLA70450":
		admite = true;
		break;
	case "MLA7037":
		admite = true;
		break;
	case "MLA58650":
		admite = true;
		break;
	case "MLA47508":
		admite = true;
		break;
	case "MLA70466":
		admite = true;
		break;
	case "MLA11395":
		admite = true;
		break;
	case "MLA30140":
		admite = true;
		break;
	case "MLA407978":
		admite = true;
		break;
	case "MLA4184":
		admite = true;
		break;
	case "MLA86877":
		admite = true;
		break;
	case "MLA3528":
		admite = true;
		break;
	case "MLA39121":
		admite = true;
		break;
	case "MLA70493":
		admite = true;
		break;
	case "MLA352681":
		admite = true;
		break;
	case "MLA73011":
		admite = true;
		break;
	case "MLA70522":
		admite = true;
		break;
	case "MLA70461":
		admite = true;
		break;
	case "MLA401279":
		admite = true;
		break;
	case "MLA393394":
		admite = true;
		break;
	case "MLA351800":
		admite = true;
		break;
	case "MLA70399":
		admite = true;
		break;
	case "MLA11809":
		admite = true;
		break;
	case "MLA112291":
		admite = true;
		break;
	case "MLA58656":
		admite = true;
		break;
	case "MLA348582":
		admite = true;
		break;
	case "MLA401580":
		admite = true;
		break;
	case "MLA393453":
		admite = true;
		break;
	case "MLA393395":
		admite = true;
		break;
	case "MLA371470":
		admite = true;
		break;
	case "MLA352027":
		admite = true;
		break;
	case "MLA346797":
		admite = true;
		break;
	case "MLA7214":
		admite = true;
		break;
	case "MLA393554":
		admite = true;
		break;
	case "MLA346346":
		admite = true;
		break;
	case "MLA393396":
		admite = true;
		break;
	case "MLA393443":
		admite = true;
		break;
	case "MLA13000":
		admite = true;
		break;
	case "MLA70480":
		admite = true;
		break;
	case "MLA86891":
		admite = true;
		break;
	case "MLA350079":
		admite = true;
		break;
	case "MLA393454":
		admite = true;
		break;
	case "MLA86915":
		admite = true;
		break;
	case "MLA70365":
		admite = true;
		break;
	case "MLA86934":
		admite = true;
		break;
	case "MLA348352":
		admite = true;
		break;
	case "MLA86869":
		admite = true;
		break;
	case "MLA406105":
		admite = true;
		break;
	case "MLA408009":
		admite = true;
		break;
	case "MLA346798":
		admite = true;
		break;
	case "MLA407007":
		admite = true;
		break;
	case "MLA407010":
		admite = true;
		break;
	case "MLA32042":
		admite = true;
		break;
	case "MLA407009":
		admite = true;
		break;
	case "MLA10686":
		admite = true;
		break;
	case "MLA70361":
		admite = true;
		break;
	case "MLA401598":
		admite = true;
		break;
	case "MLA401589":
		admite = true;
		break;
	case "MLA406080":
		admite = true;
		break;
	case "MLA70327":
		admite = true;
		break;
	case "MLA406077":
		admite = true;
		break;
	case "MLA406114":
		admite = true;
		break;
	case "MLA406081":
		admite = true;
		break;
	case "MLA406082":
		admite = true;
		break;
	case "MLA351994":
		admite = true;
		break;
	case "MLA406102":
		admite = true;
		break;
	case "MLA70504":
		admite = true;
		break;
	case "MLA70500":
		admite = true;
		break;
	case "MLA70396":
		admite = true;
		break;
	case "MLA349921":
		admite = true;
		break;
	case "MLA351974":
		admite = true;
		break;
	case "MLA406062":
		admite = true;
		break;
	case "MLA407026":
		admite = true;
		break;
	case "MLA406076":
		admite = true;
		break;
	case "MLA406110":
		admite = true;
		break;
	case "MLA406058":
		admite = true;
		break;
	case "MLA406100":
		admite = true;
		break;
	case "MLA7890":
		admite = true;
		break;
	case "MLA407044":
		admite = true;
		break;
	case "MLA349918":
		admite = true;
		break;
	case "MLA70460":
		admite = true;
		break;
	case "MLA70401":
		admite = true;
		break;
	case "MLA351973":
		admite = true;
		break;
	case "MLA406056":
		admite = true;
		break;
	case "MLA406079":
		admite = true;
		break;
	case "MLA70463":
		admite = true;
		break;
	case "MLA406104":
		admite = true;
		break;
	case "MLA406078":
		admite = true;
		break;
	case "MLA406099":
		admite = true;
		break;
	case "MLA86862":
		admite = true;
		break;
	case "MLA406064":
		admite = true;
		break;
	case "MLA406061":
		admite = true;
		break;
	case "MLA406063":
		admite = true;
		break;
	case "MLA407008":
		admite = true;
		break;
	case "MLA406106":
		admite = true;
		break;
	case "MLA70486":
		admite = true;
		break;
	case "MLA406060":
		admite = true;
		break;
	case "MLA406057":
		admite = true;
		break;
	case "MLA407011":
		admite = true;
		break;
	case "MLA407053":
		admite = true;
		break;
	case "MLA406072":
		admite = true;
		break;
	case "MLA406069":
		admite = true;
		break;
	case "MLA406073":
		admite = true;
		break;
	case "MLA406070":
		admite = true;
		break;
	case "MLA406071":
		admite = true;
		break;
	case "MLA407035":
		admite = true;
		break;
	case "MLA406065":
		admite = true;
		break;
	case "MLA406066":
		admite = true;
		break;
	case "MLA406067":
		admite = true;
		break;
	case "MLA406087":
		admite = true;
		break;
	case "MLA406090":
		admite = true;
		break;
	case "MLA406085":
		admite = true;
		break;
	case "MLA406094":
		admite = true;
		break;
	case "MLA406088":
		admite = true;
		break;
	case "MLA407071":
		admite = true;
		break;
	case "MLA406086":
		admite = true;
		break;
	case "MLA406089":
		admite = true;
		break;
	case "MLA406083":
		admite = true;
		break;
	case "MLA406075":
		admite = true;
		break;
	case "MLA10596":
		admite = true;
		break;
	case "MLA407006":
		admite = true;
		break;
	case "MLA407013":
		admite = true;
		break;
	case "MLA349919":
		admite = true;
		break;
	case "MLA407012":
		admite = true;
		break;
	case "MLA86946":
		admite = true;
		break;
	case "MLA30150":
		admite = true;
		break;
	case "MLA407017":
		admite = true;
		break;
	case "MLA406991":
		admite = true;
		break;
	case "MLA406928":
		admite = true;
		break;
	case "MLA406919":
		admite = true;
		break;
	case "MLA406982":
		admite = true;
		break;
	case "MLA406964":
		admite = true;
		break;
	case "MLA406955":
		admite = true;
		break;
	case "MLA406973":
		admite = true;
		break;
	case "MLA406937":
		admite = true;
		break;
	case "MLA406116":
		admite = true;
		break;
	case "MLA406117":
		admite = true;
		break;
	case "MLA406109":
		admite = true;
		break;
	case "MLA406112":
		admite = true;
		break;
	case "MLA406118":
		admite = true;
		break;
	case "MLA406119":
		admite = true;
		break;
	case "MLA407062":
		admite = true;
		break;
	case "MLA406111":
		admite = true;
		break;
	case "MLA406092":
		admite = true;
		break;
	case "MLA406108":
		admite = true;
		break;
	case "MLA406115":
		admite = true;
		break;
	case "MLA406091":
		admite = true;
		break;
	case "MLA406101":
		admite = true;
		break;
	case "MLA406097":
		admite = true;
		break;
	case "MLA406095":
		admite = true;
		break;
	case "MLA406093":
		admite = true;
		break;
	case "MLA407080":
		admite = true;
		break;
	case "MLA406098":
		admite = true;
		break;
	case "MLA48535":
		admite = true;
		break;
	case "MLA86958":
		admite = true;
		break;
	case "MLA70508":
		admite = true;
		break;
	case "MLA66781":
		admite = true;
		break;
	case "MLA16538":
		admite = true;
		break;
	case "MLA30147":
		admite = true;
		break;
	case "MLA64214":
		admite = true;
		break;
		//Fin Celulares y Smartphones.

	}

	callback(admite);	
}
