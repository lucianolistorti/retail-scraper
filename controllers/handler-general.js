
var scraperFunctions = require('../scraper/scraper-functions.js');
var Categoria = require("../models/categoria").Categoria;
var Producto = require("../models/producto").Producto;
var Busqueda = require("../models/busqueda").Busqueda;
var swtichWebs = require('./switch-webs');
var switchMarcas = require('./switch-marcas');
var swtichAdmitidos = require('./switch-admitidos');
var request = require('request');
var async = require('async');

const THRESHOLD_PROBABILIDAD = 0.3;

function requestCategoryPredictor(busqueda,callback){
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	
	var url = "https://api.mercadolibre.com/sites/MLA/category_predictor/predict?title=" + busquedaURL;

	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestCategoryPredictor(busqueda,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback("");
			console.log('Status:', res.statusCode);
		} else {
			callback(data);
		}
	});
}

function requestCategory(categoria, callback){
	
	var url = "https://api.mercadolibre.com/categories/" + categoria;

	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestCategory(categoria,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback("");
			console.log('Status:', res.statusCode);
		} else {
			callback(data);
		}
	});
}

function requestCategoryPredictorSearch(busqueda,callback){
	//Category predictor usando el primer resultado de ML.
	requestSearch(busqueda, (data) => {
		
		try {
			var primerResult = data.results[0];
			
			var categoria = primerResult.category_id;
			console.log("La categoría es "+ categoria);
			
			requestCategory(categoria, (data) => {
				callback(data);
			});
		}
		catch(err) {
			console.log("No results found for "+ busqueda);
			callback({});
		}
		
	});

}


function requestSearch(busqueda,callback){
	
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	
	var url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busquedaURL;
	
	request.get({
		url: url,
		timeout: 10000,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestSearch(busqueda,callback);
			console.log('Error:', err);
		} 
		else {
		
			if (res.statusCode !== 200) {
				callback("");
				console.log('Status:', res.statusCode);
			} 
			else {
				callback(data);
			}
		}
		});
}

function predecirCategoria(busqueda,esBusqueda,callback){

	busqueda = busqueda.toLowerCase();
	
	Producto.findOne({name: busqueda}, (err, producto) => {
		//Se realiza el request si no esta en la base de datos.
		if(producto){
			callback(producto.categories);
			console.log("esta el producto");
		}
		else {
			console.log("no esta el producto");
			if(esBusqueda){
				requestSearch(busqueda, (data) => {
					var filtrosML = data.filters;
					var categorias = [];
					
					if(filtrosML.length > 0){
						console.log("FILTRO: ") + filtrosML;
						for(var i=0; i<filtrosML.length; i++){
							if(filtrosML[i].id.includes("category")){
								categorias = filtrosML[i].values[0].path_from_root;
							}	
						}
						callback(categorias);
						guardarProducto(busqueda,categorias);
					}
					else {
						console.log("sin filtros, entra en category")
						requestCategoryPredictor(busqueda, (data) => {
							categorias = data.path_from_root;
							callback(categorias);
							guardarProducto(busqueda,categorias);
						});
					}	
				});
			}
			else {
				requestCategoryPredictorSearch(busqueda, (data) => {
					try {
						var categorias = data.path_from_root;
						if(categorias.length > 0){
							callback(categorias);
							guardarProducto(busqueda,categorias);
						}
						else {
							//Intento con search.
							requestSearch(busqueda, (data) => {
								var categorias = data.path_from_root;
								callback(categorias);
								guardarProducto(busqueda,categorias);
							});
						}
					}
					catch (err){
						callback([]);
					}
					
				});
			}
		}
	});
}
		/*
	Producto.findOne({name: busqueda}, (err, producto) => {
	//Se realiza el request si no esta en la base de datos.
		if(producto){
			console.log("esta el producto");
			callback(producto.categories);
		}
		else {
			console.log("no esta el producto");
			//Reemplazar vocales y &.
			var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
			
			var url = "https://api.mercadolibre.com/sites/MLA/category_predictor/predict?title=" + busquedaURL;

			request.get({
				url: url,
				json: true,
				headers: {'User-Agent': 'request'}
			}, (err, res, data) => {
				var arrayCategorias = [];
				if (err) {
					console.log('Error:', err);
					requestCategoryPredictor(busqueda,esBusqueda,callback);
				} else if (res.statusCode !== 200) {
					console.log('Status:', res.statusCode);
				} else {
					
					var arrayHaciaRoot = data.path_from_root;
					
					if(arrayCategorias.length === 0){
						//No se pudo predecir la categoria. Buscar en search.
						url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busquedaURL;
						request.get({
							url: url,
							json: true,
							headers: {'User-Agent': 'request'}
						}, (err, res, data) => {
							var arrayCategorias = [];
							if (err) {
								requestCategoryPredictor(busqueda,esBusqueda,callback);
								console.log('Error:', err);
							} else if (res.statusCode !== 200) {
								console.log('Status:', res.statusCode);
								callback(arrayCategorias);
							} else {
								var filtrosML = data.filters;
								
								for(var i=0; i<filtrosML.length; i++){
									if(filtrosML[i].id.includes("category")){
										arrayCategorias = filtrosML[i].values[0].path_from_root;
									}	
								}
								guardarProducto(busqueda,arrayCategorias);
								callback(arrayCategorias);
							}
						});
					}
					else {
						guardarProducto(busqueda,arrayCategorias);
						callback(arrayCategorias);
					}
				}
			});
		}
	});	*/


function guardarProducto(busqueda,categorias){
	console.log("se va a guardar " + busqueda);
	Producto.findOne({ name: busqueda}, (err,producto) => {
		if(!producto){
			var prod = new Producto({
				name: busqueda,
				categories: categorias
			});
			prod.save(function(err){
				if(err){
					console.log(String(err));
				}
				else {
					console.log("Datos guardados");
				}
			});
		}
	} );
}

function obtenerFiltros(busqueda,callback){
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	var url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busqueda;

	var filtros = [];
	var filtrosDisponibles = [];
	
	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		var arrayCategorias = [];
		if (err) {
			obtenerFiltros(busqueda,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback([], []);
			console.log('Status:', res.statusCode);
		} else {
			var filtrosML = data.filters;
			var filtrosDisponiblesML = data.available_filters;
			
			
			for(var i=0; i<filtrosML.length; i++){
				if(filtrosML[i].id.includes("AMLA")){
					filtros.push({
						nombre : filtrosML[i].name,
						valores : filtrosML[i].values
					});
				}	
			}
			
			for(var i=0; i<filtrosDisponiblesML.length; i++){
				if(filtrosDisponiblesML[i].id.includes("AMLA")){
					filtrosDisponibles.push({
						nombre : filtrosDisponiblesML[i].name,
						valores : filtrosDisponiblesML[i].values
					});
				}	
			}

			callback(filtros, filtrosDisponibles);
		}
	});
}

function esFiltroDisponible(nombreCategoria, filtrosDisponibles, callback){
	var valor = false;
	for(var i=0; i<filtrosDisponibles.length; i++){
		if(filtrosDisponibles[i].name === nombreCategoria){
			valor = true;
			break;
		}
	}
	callback(valor);
}


function estaContenido(i,arrayAdmitidos,producto,arrayBusqueda,arrayProducto,filtrosDisponibles,callback){

	if(i<arrayBusqueda.length){
		if( arrayBusqueda[i].id !== arrayProducto[i].id ){
			//Si entra ya no esta contenido.

			//console.log(arrayBusqueda[i].name +"---"+ arrayProducto[i].name );
			//console.log(arrayBusqueda[i].prediction_probability +"---"+ arrayProducto[i].prediction_probability );
			if(arrayAdmitidos.includes(arrayBusqueda[i].id)){
				//Son "parecidos", puede incluirse.
				//console.log("Parecidos: " + arrayBusqueda[i].name +"---"+ arrayProducto[i].name );
				//console.log(arrayBusqueda[i].prediction_probability +"---"+ arrayProducto[i].prediction_probability );
				callback(true);
			}
			else {
				//Verificar si es un filtro disponible.
				var valor = false;
				//Recorro filtros.
				for(var j=0; j<filtrosDisponibles.length; j++){
					if(valor === false){
						//Recorro nombres de categorías del filtro.
						for(var k=0; k<filtrosDisponibles[j].valores.length; k++){
							if(arrayProducto[i].name === filtrosDisponibles[j].valores[k].name){
								valor = true;
								break;
							}
						}
					}
					else {
						break;
					}
				}
				if(valor){
					callback(true);
				}
				else {
					callback(false);
				}
			}
		}
		else {
			estaContenido(i+1,arrayAdmitidos,producto,arrayBusqueda,arrayProducto,filtrosDisponibles,callback);
		}
	}
	else {
		//Termino de recorrer, esta contenido.
		callback(true);
	}
}

function aplicarFiltros(filtros, categoriasProducto, callback){
	var result = true;
	for(var i=0; i<filtros.length; i++){
		if(result){
			for(var j=0; j<categoriasProducto.length; j++){
				if(categoriasProducto[j].name === filtros[i].valores[0].name){
					result = true;
					break;
				}
				else {
					result = false;
				}
			}
		}
		else {
			break;
		}
	}
	callback(result);
}

function obtenerMarca(i,arrayCategorias,arrayPadreMarcas,callback){
	if(i<arrayCategorias.length){
		if(arrayPadreMarcas.includes(arrayCategorias[i].id)){
			//Es padre marca.
			callback(arrayCategorias[i+1].name);
		}
		else {
			obtenerMarca(i+1,arrayCategorias,arrayPadreMarcas,callback);
		}
	}
	else {
		callback("");
	}
}



function filtrarResultadosPorCategoria(busqueda, jsonProductos, callback){

	//Recorre recursivamente todos los resultados y los agrega segun su prediccion de categoria.

	//Array de categorias de la busqueda.
	var jsonResultados = {
			query: busqueda,
			categories: [],
			resultados: [],
			filtros: [],
			filtros_disponibles:[]
	}; 
	//Funciones que se ejecutaran.
	var arrayFunciones = [];

	predecirCategoria(busqueda, true, (arrayPathBusqueda) => {
		try {
			if(arrayPathBusqueda.length > 0){
				console.log(arrayPathBusqueda);
				jsonResultados.categories = arrayPathBusqueda;
				obtenerFiltros(busqueda, (filtros, filtrosDisponibles) => {
					//console.log(JSON.stringify(filtros));
					//console.log(JSON.stringify(filtrosDisponibles));
					jsonResultados.filtros = filtros;
					jsonResultados.filtros_disponibles = filtrosDisponibles;
	
					for(var i=0; i<jsonProductos.length; i++){
						var param = {
								filtros: jsonResultados.filtros,
								filtros_disponibles: jsonResultados.filtros_disponibles,
								arrayPathBusqueda: arrayPathBusqueda,
								producto: jsonProductos[i],
								jsonResultados: jsonResultados
						};
						arrayFunciones.push(evaluarProducto.bind(null, param));
					}
	
					//Realizo todas las funciones de evaluacion en paralelo. Cuando termina, se devuelve.
					async.parallel(arrayFunciones, function(err, results) {
						callback(jsonResultados);	
					});
				});
			}
			else {
				callback(jsonResultados);
				console.log("No se puede predecir la categoria.");
			}
		}
		catch (err){
			callback(jsonResultados);
			console.log("No se puede predecir la categoria.");
		}
	});
	
}


function guardarBusqueda(params){
	console.log("guardando "+params.title);
	var busqueda = new Busqueda({
		title: params.title,
		id_category: params.id_category,
		searches: 1,
		valores: params.valores
	});
	busqueda.save(function(err){
		if(err){
			console.log(String(err));
		}
		else {
			console.log("Datos guardados");
		}
	});
}

module.exports.filtrarResultadosPorCategoria = filtrarResultadosPorCategoria; 

function evaluarProducto(param,callback){
	predecirCategoria(param.producto.titulo, false, function(arrayPathProducto){
		if(arrayPathProducto.length > 0){
			param.producto.array_categorias = arrayPathProducto;
			Categoria.findOne({ id_ML: param.arrayPathBusqueda[0].id }, function(err,categoria){
				if(err){
					callback();
					console.log("Error busqueda mongoDB "+ err);
				}
				else{
					var filtrosDisponibles = param.filtros_disponibles;
					estaContenido(0,categoria.list_admited,param.producto.titulo,param.arrayPathBusqueda,arrayPathProducto,filtrosDisponibles,function(valor){
						if(valor === true){
							aplicarFiltros(param.filtros, arrayPathProducto, (result) => {
								if(result === true){
									obtenerMarca(1,param.producto.array_categorias, categoria.list_brands, function(marca){
										param.producto.marca = marca;
										param.jsonResultados.resultados.push(param.producto);
										callback();
									});	
								}
								else {
									console.log("La categoría es:" + JSON.stringify(param.producto.array_categorias));
									console.log("Se descartó "+ param.producto.titulo);
									callback();
								}
							});	
						}
						else {
							console.log("La categoría es:" + JSON.stringify(param.producto.array_categorias));
							console.log("Se descartó "+ param.producto.titulo);
							callback();
						}
					});
				}
			});
		}
		else{	
			console.log("Error, se descarto "+ param.producto.titulo);
			callback();
		}
	});
}
//Predice la categoria de la busqueda utilizando la API de Mercado Libre.
function categoriaRoot(terminoBusqueda, callback){

	Busqueda.findOne({ title: terminoBusqueda }, function(err,busqueda){

		//No esta en la base de datos. Buscar en API ML.
		if(!busqueda){
			
			predecirCategoria(terminoBusqueda,true,(categoriasBusqueda) =>{
				id = categoriasBusqueda[0].id;
				console.log(id);
				callback(id);

				Categoria.incrementarSearches(id, function(err,busqueda){
				});
			});
		}
		else{
			//Está en la base de datos.

			var id = busqueda.id_category;

			callback(id);

			//Incrementar y guardar.
			Busqueda.incrementarSearches(busqueda.title, function(err,busqueda){

			});

			//Incrementar cantidad de busquedas en categoría.
			Categoria.incrementarSearches(id, function(err,busqueda){
			});	
		}
	});
}


function definirArrayWebs(req, jsonProductos, callback){
	//Se debe obtener la categoría de la busqueda y con ella definir el array de webs.

	//Case que mapea el string del nombre de la web obtenido de la base de datos con su funcion definida arriba.
	//Ver de refactorizar.
	function caseArrayFunciones(arrayString, callback){

		var param = {
				busqueda: req.query.busqueda.toLowerCase(),
				jsonProductos: jsonProductos
		};

		var arrayFunciones = [];

		for(var i=0; i<arrayString.length; i++){
			swtichWebs.devolverFuncionWeb(arrayString[i], callbackSwitch);
		}

		function callbackSwitch(funcion){
			arrayFunciones.push(funcion.bind(null,param));
		}

		callback(arrayFunciones);
	}


	categoriaRoot(req.query.busqueda.toLowerCase(), (idCategoria) => {
		
		console.log(idCategoria);
		var arrayWebs = [];

		Categoria.findOne({id_ML: idCategoria}, function(err, categoria){
			console.log(categoria.name);

			var arrayWebsString = categoria.list_of_webs;

			caseArrayFunciones(arrayWebsString, (arrayFunciones) => {
				callback(arrayFunciones);
			});
		});	 
	});

}


//Ejecuta los requests y los parseos a las webs de forma paralela.
function requests(req, res, jsonProductos, arrayWebs){

	//arrayWebs ya contiene todas las funciones que deben ejecutarse.
	async.parallel(arrayWebs, function(err, results) {
		console.log(JSON.stringify(jsonProductos));
		filtrarResultadosPorCategoria(req.query.busqueda.toLowerCase(), jsonProductos, function(jsonResultados){
			jsonResultados.resultados.sort(function(a,b){
				return a.precio - b.precio;
			});
			console.log("sale");
			
			Categoria.findOne({id_ML: jsonResultados.categories[0].id}, (err,categoria)=>{
				var data = {
						valores: jsonResultados,
						filtros_aplicados: [],
						filtro_precio: "",
						filtro_sitios: [],
						sitios_disponibles: categoria.list_of_webs
				}
				
				res.send(data);
			})
			
			
			console.log(JSON.stringify(jsonResultados));
			var params = {
					title: jsonResultados.query,
					id_category: jsonResultados.categories[0].id,
					valores: jsonResultados
			}
			guardarBusqueda(params);
		});

	});
}


//Lógica para ir haciendo scraping de acuerdo a la categoria de la busqueda.
exports.iniciarBusquedas = function(req,res){
	if(req.query.busqueda){
		var query = req.query.busqueda.toLowerCase();
		Busqueda.findOne({ title: query }, (err,busqueda) => {
			//La busqueda esta en la base de datos, no se hacen requests.
			if(!err){
				if(busqueda){
					
					var filtrosAplicados = [];
					
					if(req.query.filtros){
						filtrosAplicados = JSON.parse(req.query.filtros);
					}
					
					var filtroPrecio = "";
					
					if(req.query.precio){
						filtroPrecio = req.query.precio;
					}
					
					var filtroSitios = "";
					
					if(req.query.no_sitios){
						filtroSitios = req.query.no_sitios;
					}
					
					Categoria.findOne({id_ML: busqueda.id_category}, (err,categoria) => {
						if((!err) && (categoria)){
							
							var sitiosDisponibles = categoria.list_of_webs;
							console.log("sitios:"+sitiosDisponibles);
								
							var data = {
									valores: busqueda.valores,
									sitios_disponibles: sitiosDisponibles,
									filtros_aplicados: filtrosAplicados,
									filtro_precio: filtroPrecio,
									filtro_sitios: filtroSitios,
							};
							console.log("Data response: " + JSON.stringify(data));
							res.send(data);
						}
						
					});	
				}
				else { //Realizar requests.
					var jsonProductos = [];
					//El array de webs indica todas las webs a las que se debe hacer scraping.
					definirArrayWebs(req, jsonProductos, (arrayWebs) => {
						//Una vez que se tiene el array se realizan las request.
						requests(req, res, jsonProductos, arrayWebs);
					});
				}
			}
			else {
				console.log("Error en la busqueda: "+ err);
			}
		});
	}
};