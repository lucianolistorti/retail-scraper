/* Estos selectores sirven para definir las categorias que tienen marcas como hijos.  
 * Para mayor legilibilidad y para acortar el scope, se dividen los switch por categorías. */

module.exports.switchGeneral = function(idCategoria,callback){
	switch(idCategoria) {
	case "MLA1051":
		callback(switchCelularesYSmartphones);
		break;
	case "MLA5726":
		callback(switchElectrodomesticos);
		break;
	case "MLA1000":
		callback(switchElectronicaAudioVideo);
		break;
	}
	
};

function switchElectronicaAudioVideo(idML,callback){

	var esPadreMarca = false;

	switch(idML) {
	case "MLA410298":
		esPadreMarca = true;
		break;
	case "MLA1003":
		esPadreMarca = true;
		break;
	case "MLA411070":
		esPadreMarca = true;
		break;
	case "MLA11889":
		esPadreMarca = true;
		break;
	case "MLA125393":
		esPadreMarca = true;
		break;
	case "MLA82086":
		esPadreMarca = true;
		break;
	case "MLA1061":
		esPadreMarca = true;
		break;
	case "MLA2675":
		esPadreMarca = true;
		break;
	case "MLA2857":
		esPadreMarca = true;
		break;
	case "MLA389772":
		esPadreMarca = true;
		break;
	case "MLA390445":
		esPadreMarca = true;
		break;
	case "MLA390464":
		esPadreMarca = true;
		break;
	case "MLA389791":
		esPadreMarca = true;
		break;
	case "MLA391523":
		esPadreMarca = true;
		break;
	case "MLA390483":
		esPadreMarca = true;
		break;
	case "MLA389810":
		esPadreMarca = true;
		break;
	case "MLA391084":
		esPadreMarca = true;
		break;
	case "MLA125270":
		esPadreMarca = true;
		break;
	case "MLA125275":
		esPadreMarca = true;
		break;
	case "MLA410311":
		esPadreMarca = true;
		break;
	case "MLA391291":
		esPadreMarca = true;
		break;
	case "MLA324178":
		esPadreMarca = true;
		break;
	case "MLA94880":
		esPadreMarca = true;
		break;
	case "MLA389365":
		esPadreMarca = true;
		break;
	case "MLA125271":
		esPadreMarca = true;
		break;
	case "MLA94883":
		esPadreMarca = true;
		break;
	case "MLA94884":
		esPadreMarca = true;
		break;
	case "MLA125274":
		esPadreMarca = true;
		break;
	case "MLA125273":
		esPadreMarca = true;
		break;
	case "MLA124893":
		esPadreMarca = true;
		break;
	case "MLA410495":
		esPadreMarca = true;
		break;
	case "MLA324292":
		esPadreMarca = true;
		break;
	case "MLA389487":
		esPadreMarca = true;
		break;
	case "MLA322982":
		esPadreMarca = true;
		break;
	case "MLA390597":
		esPadreMarca = true;
		break;
	case "MLA389903":
		esPadreMarca = true;
		break;
	case "MLA389922":
		esPadreMarca = true;
		break;
	case "MLA390578":
		esPadreMarca = true;
		break;
	case "MLA390559":
		esPadreMarca = true;
		break;
	case "MLA391293":
		esPadreMarca = true;
		break;
	case "MLA410313":
		esPadreMarca = true;
		break;
	case "MLA391087":
		esPadreMarca = true;
		break;
	case "MLA391085":
		esPadreMarca = true;
		break;
	case "MLA391086":
		esPadreMarca = true;
		break;
	case "MLA390034":
		esPadreMarca = true;
		break;
	case "MLA323404":
		esPadreMarca = true;
		break;
	case "MLA389622":
		esPadreMarca = true;
		break;
	case "MLA391294":
		esPadreMarca = true;
		break;
	case "MLA389367":
		esPadreMarca = true;
		break;
	case "MLA81536":
		esPadreMarca = true;
		break;
	case "MLA410314":
		esPadreMarca = true;
		break;
	case "MLA324102":
		esPadreMarca = true;
		break;
	case "MLA323423":
		esPadreMarca = true;
		break;
	case "MLA391292":
		esPadreMarca = true;
		break;
	case "MLA390521":
		esPadreMarca = true;
		break;
	case "MLA324216":
		esPadreMarca = true;
		break;
	case "MLA410496":
		esPadreMarca = true;
		break;
	case "MLA389364":
		esPadreMarca = true;
		break;
	case "MLA390053":
		esPadreMarca = true;
		break;
	case "MLA390072":
		esPadreMarca = true;
		break;
	case "MLA390179":
		esPadreMarca = true;
		break;
	case "MLA410491":
		esPadreMarca = true;
		break;
	case "MLA390540":
		esPadreMarca = true;
		break;
	case "MLA390312":
		esPadreMarca = true;
		break;
	case "MLA324064":
		esPadreMarca = true;
		break;
	case "MLA390502":
		esPadreMarca = true;
		break;
	case "MLA391519":
		esPadreMarca = true;
		break;
	case "MLA391290":
		esPadreMarca = true;
		break;
	case "MLA321623":
		esPadreMarca = true;
		break;
	case "MLA390331":
		esPadreMarca = true;
		break;
	case "MLA8800":
		esPadreMarca = true;
		break;
	case "MLA8802":
		esPadreMarca = true;
		break;
	case "MLA8805":
		esPadreMarca = true;
		break;
	case "MLA323328":
		esPadreMarca = true;
		break;
	case "MLA323347":
		esPadreMarca = true;
		break;
	case "MLA14874":
		esPadreMarca = true;
		break;
	case "MLA410492":
		esPadreMarca = true;
		break;
	case "MLA410308":
		esPadreMarca = true;
		break;
	case "MLA389369":
		esPadreMarca = true;
		break;
	case "MLA391287":
		esPadreMarca = true;
		break;
	case "MLA391520":
		esPadreMarca = true;
		break;
	case "MLA390255":
		esPadreMarca = true;
		break;
	case "MLA389506":
		esPadreMarca = true;
		break;
	case "MLA389468":
		esPadreMarca = true;
		break;
	case "MLA323560":
		esPadreMarca = true;
		break;
	case "MLA11899":
		esPadreMarca = true;
		break;
	case "MLA391525":
		esPadreMarca = true;
		break;
	case "MLA389884":
		esPadreMarca = true;
		break;
	case "MLA391080":
		esPadreMarca = true;
		break;
	case "MLA324026":
		esPadreMarca = true;
		break;
	case "MLA323001":
		esPadreMarca = true;
		break;
	case "MLA323579":
		esPadreMarca = true;
		break;
	case "MLA389362":
		esPadreMarca = true;
		break;
	case "MLA390426":
		esPadreMarca = true;
		break;
	case "MLA410498":
		esPadreMarca = true;
		break;
	case "MLA390236":
		esPadreMarca = true;
		break;
	case "MLA390217":
		esPadreMarca = true;
		break;
	case "MLA391526":
		esPadreMarca = true;
		break;
	case "MLA390407":
		esPadreMarca = true;
		break;
	case "MLA391524":
		esPadreMarca = true;
		break;
	case "MLA410312":
		esPadreMarca = true;
		break;
	case "MLA389430":
		esPadreMarca = true;
		break;
	case "MLA391079":
		esPadreMarca = true;
		break;
	case "MLA322887":
		esPadreMarca = true;
		break;
	case "MLA389397":
		esPadreMarca = true;
		break;
	case "MLA390198":
		esPadreMarca = true;
		break;
	case "MLA390160":
		esPadreMarca = true;
		break;
	case "MLA322868":
		esPadreMarca = true;
		break;
	case "MLA123863":
		esPadreMarca = true;
		break;
	case "MLA391286":
		esPadreMarca = true;
		break;
	case "MLA389449":
		esPadreMarca = true;
		break;
	case "MLA389368":
		esPadreMarca = true;
		break;
	case "MLA391289":
		esPadreMarca = true;
		break;
	case "MLA410309":
		esPadreMarca = true;
		break;
	case "MLA410493":
		esPadreMarca = true;
		break;
	case "MLA391521":
		esPadreMarca = true;
		break;
	case "MLA321527":
		esPadreMarca = true;
		break;
	case "MLA323039":
		esPadreMarca = true;
		break;
	case "MLA125272":
		esPadreMarca = true;
		break;
	case "MLA323309":
		esPadreMarca = true;
		break;
	case "MLA410310":
		esPadreMarca = true;
		break;
	case "MLA389366":
		esPadreMarca = true;
		break;
	case "MLA323290":
		esPadreMarca = true;
		break;
	case "MLA391083":
		esPadreMarca = true;
		break;
	case "MLA391522":
		esPadreMarca = true;
		break;
	case "MLA389660":
		esPadreMarca = true;
		break;
	case "MLA410497":
		esPadreMarca = true;
		break;
	case "MLA390388":
		esPadreMarca = true;
		break;
	case "MLA323271":
		esPadreMarca = true;
		break;
	case "MLA390274":
		esPadreMarca = true;
		break;
	case "MLA391082":
		esPadreMarca = true;
		break;
	case "MLA389584":
		esPadreMarca = true;
		break;
	case "MLA323020":
		esPadreMarca = true;
		break;
	case "MLA389546":
		esPadreMarca = true;
		break;
	case "MLA389735":
		esPadreMarca = true;
		break;
	case "MLA410494":
		esPadreMarca = true;
		break;
	case "MLA389697":
		esPadreMarca = true;
		break;
	case "MLA39385":
		esPadreMarca = true;
		break;
	case "MLA2829":
		esPadreMarca = true;
		break;
	case "MLA389641":
		esPadreMarca = true;
		break;
	case "MLA324140":
		esPadreMarca = true;
		break;
	case "MLA323988":
		esPadreMarca = true;
		break;
	case "MLA410307":
		esPadreMarca = true;
		break;
	case "MLA390350":
		esPadreMarca = true;
		break;
	case "MLA390369":
		esPadreMarca = true;
		break;
	case "MLA389370":
		esPadreMarca = true;
		break;
	case "MLA323252":
		esPadreMarca = true;
		break;
	case "MLA390122":
		esPadreMarca = true;
		break;
	case "MLA391493":
		esPadreMarca = true;
		break;
	case "MLA405978":
		esPadreMarca = true;
		break;
	case "MLA8723":
		esPadreMarca = true;
		break;
	case "MLA91759":
		esPadreMarca = true;
		break;
	case "MLA389565":
		esPadreMarca = true;
		break;
	case "MLA91865":
		esPadreMarca = true;
		break;
	case "MLA91864":
		esPadreMarca = true;
		break;
	case "MLA390293":
		esPadreMarca = true;
		break;
	case "MLA389716":
		esPadreMarca = true;
		break;
	case "MLA352005":
		esPadreMarca = true;
		break;
	case "MLA352006":
		esPadreMarca = true;
		break;
	case "MLA352009":
		esPadreMarca = true;
		break;
	case "MLA352010":
		esPadreMarca = true;
		break;
	case "MLA352007":
		esPadreMarca = true;
		break;
	case "MLA352008":
		esPadreMarca = true;
		break;
	}
	callback(esPadreMarca);
}

function switchElectrodomesticos(idML,callback){

	var esPadreMarca = false;

	switch(idML) {

	//Inicio Electrodomésticos y Aires
	case "MLA1577":
		esPadreMarca = true;
		break;
	case "MLA10228":
		esPadreMarca = true;
		break;
	case "MLA10705":
		esPadreMarca = true;
		break;
	case "MLA117259":
		esPadreMarca = true;
		break;
	case "MLA125876":
		esPadreMarca = true;
		break;
	case "MLA10064":
		esPadreMarca = true;
		break;
	case "MLA104680":
		esPadreMarca = true;
		break;
	case "MLA10068":
		esPadreMarca = true;
		break;
	case "MLA1579":
		esPadreMarca = true;
		break;
	case "MLA4622":
		esPadreMarca = true;
		break;
	case "MLA125567":
		esPadreMarca = true;
		break;
	case "MLA125479":
		esPadreMarca = true;
		break;
	case "MLA125428":
		esPadreMarca = true;
		break;
	case "MLA52970":
		esPadreMarca = true;
		break;
	case "MLA125478":
		esPadreMarca = true;
		break;
	case "MLA125477":
		esPadreMarca = true;
		break;
	case "MLA4773":
		esPadreMarca = true;
		break;
	case "MLA10503":
		esPadreMarca = true;
		break;
	case "MLA126301":
		esPadreMarca = true;
		break;
	case "MLA125635":
		esPadreMarca = true;
		break;
	case "MLA126302":
		esPadreMarca = true;
		break;
	case "MLA126054":
		esPadreMarca = true;
		break;
	case "MLA125433":
		esPadreMarca = true;
		break;
	case "MLA126056":
		esPadreMarca = true;
		break;
	case "MLA126053":
		esPadreMarca = true;
		break;
	case "MLA125636":
		esPadreMarca = true;
		break;
	case "MLA125637":
		esPadreMarca = true;
		break;
	case "MLA125432":
		esPadreMarca = true;
		break;
	case "MLA126055":
		esPadreMarca = true;
		break;
	case "MLA385691":
		esPadreMarca = true;
		break;
	case "MLA125434":
		esPadreMarca = true;
		break;
	case "MLA1647":
		esPadreMarca = true;
		break;
	case "MLA3677":
		esPadreMarca = true;
		break;
	case "MLA321785":
		esPadreMarca = true;
		break;
	case "MLA321784":
		esPadreMarca = true;
		break;
	case "MLA3678":
		esPadreMarca = true;
		break;
	case "MLA8896":
		esPadreMarca = true;
		break;
	case "MLA321786":
		esPadreMarca = true;
		break;
	case "MLA321783":
		esPadreMarca = true;
		break;
	case "MLA10224":
		esPadreMarca = true;
		break;
	case "MLA10102":
		esPadreMarca = true;
		break;
	case "MLA401611":
		esPadreMarca = true;
		break;
	case "MLA10098":
		esPadreMarca = true;
		break;
	case "MLA10055":
		esPadreMarca = true;
		break;
	case "MLA126207":
		esPadreMarca = true;
		break;
	case "MLA401612":
		esPadreMarca = true;
		break;
	case "MLA126210":
		esPadreMarca = true;
		break;
	case "MLA126209":
		esPadreMarca = true;
		break;
	case "MLA126208":
		esPadreMarca = true;
		break;
	case "MLA399248":
		esPadreMarca = true;
		break;
	case "MLA399249":
		esPadreMarca = true;
		break;
	case "MLA398580":
		esPadreMarca = true;
		break;
	case "MLA398606":
		esPadreMarca = true;
		break;
	case "MLA126300":
		esPadreMarca = true;
		break;
	case "MLA74278":
		esPadreMarca = true;
		break;
	case "MLA74268":
		esPadreMarca = true;
		break;
	case "MLA392009":
		esPadreMarca = true;
		break;
	case "MLA389246":
		esPadreMarca = true;
		break;
	case "MLA74279":
		esPadreMarca = true;
		break;
	case "MLA74281":
		esPadreMarca = true;
		break;
	case "MLA74286":
		esPadreMarca = true;
		break;
	case "MLA74280":
		esPadreMarca = true;
		break;
	case "MLA74282":
		esPadreMarca = true;
		break;
	case "MLA125819":
		esPadreMarca = true;
		break;
	case "MLA9457":
		esPadreMarca = true;
		break;
	case "MLA9456":
		esPadreMarca = true;
		break;
	case "MLA74269":
		esPadreMarca = true;
		break;
	case "MLA401457":
		esPadreMarca = true;
		break;
	case "MLA125568":
		esPadreMarca = true;
		break;
	case "MLA74263":
		esPadreMarca = true;
		break;
	case "MLA74265":
		esPadreMarca = true;
		break;
	case "MLA18407":
		esPadreMarca = true;
		break;
	case "MLA18376":
		esPadreMarca = true;
		break;
	case "MLA10287":
		esPadreMarca = true;
		break;
	case "MLA10237":
		esPadreMarca = true;
		break;
	case "MLA18375":
		esPadreMarca = true;
		break;
	case "MLA10282":
		esPadreMarca = true;
		break;
	case "MLA119693":
		esPadreMarca = true;
		break;
	case "MLA10111":
		esPadreMarca = true;
		break;
	case "MLA10122":
		esPadreMarca = true;
		break;
	case "MLA10073":
		esPadreMarca = true;
		break;
	}

	callback(esPadreMarca);	
}

function switchCelularesYSmartphones(idML,callback){

	var esPadreMarca = false;

	switch(idML) {

	//Inicio Celulares y Smartphones

	case "MLA5337":
		esPadreMarca = true;
		break;
	case "MLA5427":
		esPadreMarca = true;
		break;
	case "MLA1055":
		esPadreMarca = true;
		break;
	case "MLA10675":
		esPadreMarca = true;
		break;
	case "MLA10193":
		esPadreMarca = true;
		break;
	case "MLA5068":
		esPadreMarca = true;
		break;
	case "MLA4161":
		esPadreMarca = true;
		break;
	case "MLA4159":
		esPadreMarca = true;
		break;
	case "MLA70406":
		esPadreMarca = true;
		break;
	case "MLA70345":
		esPadreMarca = true;
		break;
	case "MLA7039":
		esPadreMarca = true;
		break;
	case "MLA10617":
		esPadreMarca = true;
		break;
	case "MLA5549":
		esPadreMarca = true;
		break;
	case "MLA11860":
		esPadreMarca = true;
		break;
	case "MLA32075":
		esPadreMarca = true;
		break;
	case "MLA15641":
		esPadreMarca = true;
		break;
	case "MLA37203":
		esPadreMarca = true;
		break;
	case "MLA61294":
		esPadreMarca = true;
		break;
	case "MLA10183":
		esPadreMarca = true;
		break;
	case "MLA30158":
		esPadreMarca = true;
		break;
	case "MLA3514":
		esPadreMarca = true;
		break;
	case "MLA70387":
		esPadreMarca = true;
		break;
	case "MLA5338":
		esPadreMarca = true;
		break;
	case "MLA4656":
		esPadreMarca = true;
		break;
	case "MLA352679":
		esPadreMarca = true;
		break;
	case "MLA47540":
		esPadreMarca = true;
		break;
	case "MLA401278":
		esPadreMarca = true;
		break;
	case "MLA47475":
		esPadreMarca = true;
		break;
	case "MLA2907":
		esPadreMarca = true;
		break;
	case "MLA2895":
		esPadreMarca = true;
		break;
	case "MLA2896":
		esPadreMarca = true;
		break;
	case "MLA407976":
		esPadreMarca = true;
		break;
	case "MLA10649":
		esPadreMarca = true;
		break;
	case "MLA10679":
		esPadreMarca = true;
		break;
	case "MLA10606":
		esPadreMarca = true;
		break;
	case "MLA10673":
		esPadreMarca = true;
		break;
	case "MLA92699":
		esPadreMarca = true;
		break;
	case "MLA13547":
		esPadreMarca = true;
		break;
	case "MLA405671":
		esPadreMarca = true;
		break;
	case "MLA92691":
		esPadreMarca = true;
		break;
	case "MLA405664":
		esPadreMarca = true;
		break;
	case "MLA47496":
		esPadreMarca = true;
		break;
	case "MLA407011":
		esPadreMarca = true;
		break;
	case "MLA406908":
		esPadreMarca = true;
		break;
	case "MLA406914":
		esPadreMarca = true;
		break;
	case "MLA406907":
		esPadreMarca = true;
		break;
	case "MLA406906":
		esPadreMarca = true;
		break;
	case "MLA406911":
		esPadreMarca = true;
		break;
	case "MLA406912":
		esPadreMarca = true;
		break;
	case "MLA406913":
		esPadreMarca = true;
		break;
	case "MLA406910":
		esPadreMarca = true;
		break;
	case "MLA10595":
		esPadreMarca = true;
		break;
	case "MLA407006":
		esPadreMarca = true;
		break;
	case "MLA405669":
		esPadreMarca = true;
		break;
	case "MLA12847":
		esPadreMarca = true;
		break;
	case "MLA405668":
		esPadreMarca = true;
		break;
	case "MLA47494":
		esPadreMarca = true;
		break;
	case "MLA10637":
		esPadreMarca = true;
		break;
	case "MLA13886":
		esPadreMarca = true;
		break;
	case "MLA47493":
		esPadreMarca = true;
		break;
	case "MLA92688":
		esPadreMarca = true;
		break;
	case "MLA407008":
		esPadreMarca = true;
		break;
	case "MLA13883":
		esPadreMarca = true;
		break;
	case "MLA407007":
		esPadreMarca = true;
		break;
	case "MLA92698":
		esPadreMarca = true;
		break;
	case "MLA13884":
		esPadreMarca = true;
		break;
	case "MLA92696":
		esPadreMarca = true;
		break;
	case "MLA405672":
		esPadreMarca = true;
		break;
	case "MLA405661":
		esPadreMarca = true;
		break;
	case "MLA92690":
		esPadreMarca = true;
		break;
	case "MLA405662":
		esPadreMarca = true;
		break;
	case "MLA407009":
		esPadreMarca = true;
		break;
	case "MLA405673":
		esPadreMarca = true;
		break;
	case "MLA13901":
		esPadreMarca = true;
		break;
	case "MLA92692":
		esPadreMarca = true;
		break;
	case "MLA38549":
		esPadreMarca = true;
		break;
	case "MLA10612":
		esPadreMarca = true;
		break;
	case "MLA13902":
		esPadreMarca = true;
		break;
	case "MLA92701":
		esPadreMarca = true;
		break;
	case "MLA405666":
		esPadreMarca = true;
		break;
	case "MLA47497":
		esPadreMarca = true;
		break;
	case "MLA92700":
		esPadreMarca = true;
		break;
	case "MLA92693":
		esPadreMarca = true;
		break;
	case "MLA92694":
		esPadreMarca = true;
		break;
	case "MLA38550":
		esPadreMarca = true;
		break;
	case "MLA405665":
		esPadreMarca = true;
		break;
	case "MLA405663":
		esPadreMarca = true;
		break;
	case "MLA405670":
		esPadreMarca = true;
		break;
	case "MLA13889":
		esPadreMarca = true;
		break;
	case "MLA38548":
		esPadreMarca = true;
		break;
	case "MLA407012":
		esPadreMarca = true;
		break;
	case "MLA47492":
		esPadreMarca = true;
		break;
	case "MLA92702":
		esPadreMarca = true;
		break;
	case "MLA13890":
		esPadreMarca = true;
		break;
	case "MLA47498":
		esPadreMarca = true;
		break;
	case "MLA13892":
		esPadreMarca = true;
		break;
	case "MLA13896":
		esPadreMarca = true;
		break;
	case "MLA92695":
		esPadreMarca = true;
		break;
	case "MLA92703":
		esPadreMarca = true;
		break;
	case "MLA405674":
		esPadreMarca = true;
		break;
	case "MLA407010":
		esPadreMarca = true;
		break;
	case "MLA13895":
		esPadreMarca = true;
		break;
	case "MLA407013":
		esPadreMarca = true;
		break;
	case "MLA405667":
		esPadreMarca = true;
		break;
	case "MLA13898":
		esPadreMarca = true;
		break;
	case "MLA47495":
		esPadreMarca = true;
		break;

		//Fin Celulares y Smartphones.

	}

	callback(esPadreMarca);	
}
