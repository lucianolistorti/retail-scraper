var resultados = [];
var resultadosFiltrados = [];

var infoBusqueda = [];

var filtrosAplicados = [];
var filtrosDisponibles = [];
var filtroPrecio;
var filtroSitios = [];

var cantidadResultados = 0;
var sitios = [];
var busqueda;

//Cuando este listo el documento, se realiza la busqueda por ajax.
$(function(){
	ajaxResultados();
});

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function mostrarPorSitio(producto){
	if(filtroSitios.includes(producto.web.toLowerCase())){
		return false;
	}
	else {
		return true;
	}
}

function estaContenido(filtro, categorias){
	for(var i=0; i<categorias.length; i++){
		console.log("comparando "+filtro+" --- "+categorias[i].name);
		if(categorias[i].name === filtro){
			return true;
		}
	}
	return false;
}

function filtrarResultados(){
	
	//Filtros categorías.
	filtrosAplicados = [];
	$('div.filtro-disponible input.input-filtro:checked').each(function(){
		filtrosAplicados.push($(this).val());
	});
	var urlFiltros = "";
	if(filtrosAplicados.length > 0){
		urlFiltros = "&filtros=" + JSON.stringify(filtrosAplicados);
	}
	
	//Precio.
	filtroPrecio = $('input#precio-minimo').val();
	var urlFiltroPrecio = "";
	if(filtroPrecio){
		urlFiltroPrecio = "&precio=" + filtroPrecio;
	}
	
	//Sitios
	filtroSitios = [];
	$('div.filtro-sitios input:checkbox:not(:checked)').each(function(){
		filtroSitios.push($(this).val());
	});
	var urlFiltroSitios = "";
	if(filtroSitios.length > 0){
		urlFiltroSitios = "&no_sitios=" + JSON.stringify(filtroSitios);
	}
	
	window.location.replace("http://localhost:3000/search?busqueda="+busqueda+urlFiltros+urlFiltroPrecio+urlFiltroSitios);

}

function mostrarProducto(producto){

	if(!mostrarPorPrecio(producto)){
		console.log("se filtro "+producto.titulo+" por precio.");
		return false;
	}
	else {
		if(!mostrarPorSitio(producto)){
			console.log("se filtro "+producto.titulo+" por sitio.");
			return false;
		}
		else {
			if(filtrosAplicados.length > 0){
				for(var i=0; i<filtrosAplicados.length; i++){
					//Si se cumple algun filtro se debe mostrar.
					if(estaContenido(filtrosAplicados[i], producto.array_categorias)){
						return true;
					}
				}
				//Si no se cumplió ninguno, no se debe mostrar.
				console.log("se filtro "+producto.titulo+" por categoría.");
				return false;
			}
			else {
				return true;
			}
		}
	}
}


function mostrarBuscandoAnimacion(mostrar){
	if (mostrar)
		$("p#buscando").html("Buscando... <br><br>¿Sabías que sos el primero en buscar esta combinación de palabras? Por eso, la magia de nuestros algoritmos se está tomando unos segundos más.<br><br> A veces, ser el primero tiene sus desventajas. Sé paciente y verás los resultados en segundos! :D");
	else
		$("p#buscando").text("Resultados:");
}

function mostrarPorPrecio(producto){
	if( (filtroPrecio <= producto.precio) ){
		return true;
	}
	else {
		return false;
	}
}

function mostrarResultados(){
	
	var html = "";
	
	for(var i=0; i<resultados.length; i++){
		if(mostrarProducto(resultados[i])){
			cantidadResultados++;
			html += "<div class='producto'>" +
		      	"<div class='img-producto'>" +
		      		"<a href='" + resultados[i].url + "' target='_blank'><img src='"+ resultados[i].imagen +"' width='170' height='170' alt='" + resultados[i].titulo + "' id='' data-rjs-processed='true'></a>" +
		      	"</div>" +
		      	"<div>" +
			     	"<a href='" + resultados[i].url + "' class='link-producto' target='_blank'><p class='titulo-producto'>" + resultados[i].titulo + "</p></a>" +
			     	"<p class='marca-producto'>" + resultados[i].marca + "</p>" +
		     	"</div>" +
		     	"<span class='precio-web'>" +
			     	"<p class='web-producto'>" + resultados[i].web + "</p>" +
			     	"<div class='precio-producto'>$ " + resultados[i].precio + "</div>" +
		     	"</span>" +
		     	"<!-- <span class='btn-web'><p class='visitar'>Ver producto </p></span> -->"+
		      "</div>";
		}
	}
	
	if(cantidadResultados === 0){
		mostrarNoHayResultados();
	}
	else{
		$("div.cantidad-resultados").html(cantidadResultados + " resultados");
		$("div#lista-resultados").html(html);
	}
}

function mostrarFiltrosAplicados(){
	var html = "";
	
	for(var i=0; i<filtrosAplicados.length; i++){
		html+= "<span class='filtro-aplicado'><p class='texto-filtro'>" + filtrosAplicados[i] + "</p> </span>";		
	}
	
	html += "</div>";
	
	$("div#filtros-aplicados").html(html);
}


function mostrarFiltrosDisponibles(){
	
	var html = "";
	var checked = "";
	
	for(var i=0; i<filtrosDisponibles.length; i++){
		html+= "<div class='filtro-disponible'>" +
	      "<h4>" + filtrosDisponibles[i].nombre + "</h4>" +
	      "<ul class='nav nav-pills nav-stacked'>";
	      for(var j=0; (j<10) && (j<filtrosDisponibles[i].valores.length); j++){
	    	  	//Si esta el filtro lo marco checked.
	    	  	if(filtrosAplicados.includes(filtrosDisponibles[i].valores[j].name)){
	    	  		checked = "checked='checked'";
	    	  	}
	    	  	else {
	    	  		checked = "";
	    	  	}
	    	  	html+= "<li><label class='label-filtro elem-filtro-disponible'>" + filtrosDisponibles[i].valores[j].name + "<input class='input-filtro' type='checkbox' "+checked+" value='" + filtrosDisponibles[i].valores[j].name + "'><span class='nombre-filtro'></span></label></li>";
	      }
	      
	      if(j<filtrosDisponibles[i].valores.length) {
	    	  	html+="<div id='ver-mas-filtro"+ i +"' class='collapse'>";
	    	  	for(j=j; j<filtrosDisponibles[i].valores.length; j++){
	    	  		if(filtrosAplicados.includes(filtrosDisponibles[i].valores[j].name)){
		    	  		checked = "checked='checked'";
		    	  	}
		    	  	else {
		    	  		checked = "";
		    	  	}
	    	  		html+= "<li><label class='label-filtro elem-filtro-disponible'>" + filtrosDisponibles[i].valores[j].name + "<input class='input-filtro' type='checkbox' "+checked+" value='" + filtrosDisponibles[i].valores[j].name + "'><span class='nombre-filtro'></span></label></li>";
	    	  	}
	    	  	html+= "</div>"+
	    	  		   "<a class='btn-ver-mas' data-toggle='collapse' data-target='#ver-mas-filtro"+ i +"'>Ver más</a>";
	      }
	      
	      
	      
	      html+= "</ul> " +
			  "</div>";
	}
	
	//Boton filtrar.
	html+= "<span class='btn-aplicar-filtros'>" +
			"<button class='btn btn-default' type='button' id='aplicar-filtros' onclick='filtrarResultados()'>" +
			"<span class='glyphicon glyphicon-chevron-right'></span>" +
			"</button>" +
			"</span>";
	
	$("div#filtros-disponibles").append(html);
	
	$(".btn-ver-mas").click(function(){
	  	if($(this).text() === "Ver más"){
	  		$(this).html("Ver menos");
	  	}
	  	else {
	  		$(this).html("Ver más");
	  	}
  });
}

function mostrarInfoBusqueda(){
	
	var html = "";
	
	for(var i=0; i<infoBusqueda.length; i++){
		html+= "<p class='info-busqueda'>"+ infoBusqueda[i].valores[0].name +"</p>";
	}
	
	$("span.info").html(html);
	
}

function mostrarFiltroSitios(){
	
	var html = "<div class='filtro-sitios'>" +
			    "<div class='sitios-disponibles' data-toggle='collapse' data-target='#collapse-sitios'> Sitios disponibles <span class='glyphicon glyphicon-chevron-down chevron-sitio'> </div>" +
			    "<ul class='nav nav-pills nav-stacked collapse' id='collapse-sitios'>";
	
	var checked = "";
	
	for(var i=0; i<sitios.length; i++){
		//Si esta el filtro lo marco checked.
		if(!(filtroSitios.includes(sitios[i]))){
			checked = "checked";
		}
		else {
			checked = "";
		}
		
		html+= "<li><label class='label-filtro elem-filtro-disponible'>" + sitios[i] + "<input class='input-filtro' type='checkbox' "+checked+" value='" + sitios[i] + "'><span class='nombre-filtro'></span></label></li>";
	}

	html+= "</ul> " +
	"</div>";
	
	$("div#filtros-disponibles").append(html);
}

function mostrarFiltroPrecio() {
	
	var html = "<div class='filtro-disponible'>" +
			    "<h4>Precio</h4>" +
			    "<div class='input-group' style='display: inline-flex;'>"+
			    "<span class='input-precio-minimo'><input type='number' class='form-control' id='precio-minimo' placeholder='Mínimo' value='" + filtroPrecio + "'></span>" +
			    "<span class='btn-filtrar-precio'><button class='btn btn-default' type='button' onclick='filtrarResultados()' id='filtrar-precio'>" +
			    "<span class='glyphicon glyphicon-chevron-right'></span>" +
				"</button>" +
				"</span>" +
				"</div>";
	
	$("div#filtros-disponibles").append(html);
	
}

function mostrarNoHayResultados(){
	var html = "<p> No se encontraron resultados para la búsqueda. </p>";
	$("div#lista-resultados").html(html);
}

function ajaxResultados(){
	//Realiza una petición AJAX al servidor para obtener los resultados.
	
	mostrarBuscandoAnimacion(true);
	
	busqueda = $("div.busqueda").text();
	
	if(busqueda){
		var parametros = {
				"busqueda" : busqueda,
				"filtros": getUrlParameter('filtros'),
				"precio": getUrlParameter('precio'),
				"no_sitios": getUrlParameter('no_sitios')
		};

		$.get('/search-ajax', parametros, function(data){
			
			//Obtengo todos los datos.
			resultados = data.valores.products;
			infoBusqueda = data.valores.filters;
			filtrosDisponibles = data.valores.available_filters;
			filtrosAplicados = data.filtros_aplicados;
			filtroPrecio = data.filtro_precio;
			filtroSitios = data.filtro_sitios;
			sitios = data.sitios_disponibles;
			
			mostrarBuscandoAnimacion(false);
			
			if((resultados !== undefined) && (data.valores.products.length > 0)){
				mostrarResultados();
				
				if( (infoBusqueda !== undefined) && (infoBusqueda.length > 0) ){
					mostrarInfoBusqueda();
				}
				
				//Filtro precio.
				mostrarFiltroPrecio();
				mostrarFiltroSitios();
				
				//Filtros Aplicados.
				if( (filtrosAplicados !== undefined) && (filtrosAplicados.length > 0) ){
					mostrarFiltrosAplicados();
				}
				else {
					$("div#filtros-aplicados").remove();
				}
				
				//Filtros Disponibles.
				if( (filtrosDisponibles !== undefined) && (filtrosDisponibles.length > 0) ){
					mostrarFiltrosDisponibles();
				}
			}
			
		});
	}
	else {
		//Consulta vacía.
		mostrarNoHayResultados();
	}
	
	
	
}







