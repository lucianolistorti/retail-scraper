
/*
 * GET home page.
 */

var handlerGeneral = require('../controllers/handler-general.js');

exports.index = function(req, res){
	res.render('formulario');
};

exports.searchAjax = function(req, res){
	//Se deriva al core para realizar los algoritmos de scraping.
	handlerGeneral.iniciarBusquedas(req,res);
};

exports.search = function(req, res){
	//Se redirige al layout de busqueda.
	var busqueda = req.query.busqueda;
	res.render('search', {busqueda : busqueda, layout: 'search-layout'});
};