
var scraperFunctions = require('./scraper-functions');
	
function getWebFunction(web,callback){
	switch(web) {
		case "fravega":
			callback(scraperFunctions.fravega);
			break;
		case "garbarino":
			callback(scraperFunctions.garbarino);
			break;
		case "maxihogar":
			callback(scraperFunctions.maxihogar);
			break;
		case "linio":
			callback(scraperFunctions.linio);
			break;
		case "falabella":
			callback(scraperFunctions.falabella);
			break;
		case "electropuntonet":
			callback(scraperFunctions.electropuntonet);
			break;
		case "musimundo":
			callback(scraperFunctions.musimundo);
			break;
		case "ansila":
			callback(scraperFunctions.ansila);
			break;
		case "tiomusa":
			callback(scraperFunctions.tiomusa);
			break;
		case "mercadolibre":
			callback(scraperFunctions.mercadolibre);
			break;
		case "pardo":
			callback(scraperFunctions.pardo);
			break;
		case "celutronic":
			callback(scraperFunctions.celutronic);
			break;
		case "compumundo":
			callback(scraperFunctions.compumundo);
			break;
		case "carrefour":
			callback(scraperFunctions.carrefour);
			break;
		case "cetrogar":
			callback(scraperFunctions.cetrogar);
			break;
		case "bidcom":
			callback(scraperFunctions.bidcom);
			break;
		case "beiro hogar":
			callback(scraperFunctions.beiroHogar);
			break;
		case "authogar":
			callback(scraperFunctions.authogar);
			break;
		case "electro next":
			callback(scraperFunctions.electroNext);
			break;
		case "whirlpool store":
			callback(scraperFunctions.whirlpoolStore);
			break;
		case "delta":
			callback(scraperFunctions.delta);
			break;
	}
}

module.exports.getWebFunction = getWebFunction;

		