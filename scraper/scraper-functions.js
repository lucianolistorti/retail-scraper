
var Sitio = require("../models/sitio").Sitio;
var scraper = require('./scraper');
var parseFunctions = require('./parse-functions');


//Defino funciones de las webs para inicializar valores y parsear.

//Garbarino.
module.exports.garbarino = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "garbarino" }, function(err,sitio){
			var url = sitio.url + busquedaURL + "/srch?q=" + busquedaURL;
			scraper.doScraping(param.jsonProductos, parseFunctions.parseGarbarino, url,param.busqueda, callback);
		});
		
};

//Compumundo.
module.exports.compumundo = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "compumundo" }, function(err,sitio){
			var url = sitio.url + busquedaURL + "/srch?q=" + busquedaURL;
			scraper.doScraping(param.jsonProductos, parseFunctions.parseCompumundo, url, param.busqueda,callback);
		});
		
};
	
//Fravega.
module.exports.fravega = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "fravega" }, function(err,sitio){
			var url = sitio.url + busquedaURL;
			scraper.doScraping(param.jsonProductos, parseFunctions.parseFravega, url, param.busqueda,callback);
		});
		
};
	
//Maxihogar.
module.exports.maxihogar = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "maxihogar" }, function(err,sitio){
			var url = sitio.url + busquedaURL;
			scraper.doScraping(param.jsonProductos, parseFunctions.parseMaxihogar, url, param.busqueda,callback);
		});
		
};
	
	https://www.linio.com.ar/search?q=
		
//Linio.
module.exports.linio = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "linio" }, function(err,sitio){
				var url = sitio.url + "search?q="  + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseLinio, url,param.busqueda, callback);
			});
			
};


//Falabella.
module.exports.falabella = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "falabella" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScrapingPhantom(param.jsonProductos, parseFunctions.parseFalabella, url, "https://www.falabella.com.ar/", callback);
		});
};

//Electropuntonet.
module.exports.electropuntonet = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "-");
		
		Sitio.findOne({ name: "electropuntonet" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseElectropuntonet, url,param.busqueda, callback);
		});
};

//Musimundo.
module.exports.musimundo = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "musimundo" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseMusimundo, url, param.busqueda,callback);
		});
};

//Ansila.
module.exports.ansila = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+") + "_qO" + param.busqueda.replace(" ", "+") + "xSM";
		
		Sitio.findOne({ name: "ansila" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseAnsila, url,param.busqueda, callback);
		});
};

//Tiomusa.
module.exports.tiomusa = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "-");
		
		Sitio.findOne({ name: "tiomusa" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseTiomusa, url,param.busqueda, callback);
		});
};

//Mercadolibre.
module.exports.mercadolibre = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "mercadolibre" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doAPIScraping(param.jsonProductos, parseFunctions.parseMercadolibre, url, param.busqueda,callback);
		});
};

//Pardo.
module.exports.pardo = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "pardo" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parsePardo, url, param.busqueda,callback);
		});
};

//Celutronic.
module.exports.celutronic = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+") + "&product_cat=0&post_type=product";
		
		Sitio.findOne({ name: "celutronic" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseCelutronic, url, param.busqueda, callback);
		});
};

//Carrefour.
module.exports.carrefour = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "carrefour" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseCarrefour, url, param.busqueda, callback);
		});
};

//Cetrogar.
module.exports.cetrogar = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "cetrogar" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseCetrogar, url,param.busqueda, callback);
		});
};

//Bidcom.
module.exports.bidcom = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "bidcom" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseBidcom, url,param.busqueda, callback);
		});
};

//Beiro Hogar.
module.exports.beiroHogar = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "beiro hogar" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseBeiroHogar, url,param.busqueda, callback);
		});
};

//Authogar.
module.exports.authogar = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "authogar" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseAuthogar, url,param.busqueda, callback);
		});
};

//Electro Next.
module.exports.electroNext = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		busquedaURL = busquedaURL + "_qO" + busquedaURL + "xSM";
		
		Sitio.findOne({ name: "electro next" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseElectroNext, url,param.busqueda, callback);
		});
};

//Whirlpool Store.
module.exports.whirlpoolStore = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "%20");
		
		Sitio.findOne({ name: "whirlpool store" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseWhirlpoolStore, url,param.busqueda, callback);
		});
};

//Delta.
module.exports.delta = function(param, callback){
		var busquedaURL = param.busqueda.replace(" ", "+");
		
		Sitio.findOne({ name: "delta" }, function(err,sitio){
				var url = sitio.url + busquedaURL;
				scraper.doScraping(param.jsonProductos, parseFunctions.parseDelta, url,param.busqueda, callback);
		});
};
	
	//Fin definición de funciones.
	