/**
 * Algoritmos de parseo particulares de cada web.
 */


//.class, #id

String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement+ this.substr(index + replacement.length);
};

function toFloatOrdenableIngles(texto){
	//Convertir los string que se sacan de las webs, a un float para que se pueda ordenar.
	if(texto !== undefined){
		return parseFloat(texto.replace("$", "").replace(",","").replace(/^[a-zA-Z]*$/,""));
	}
}

function toFloatOrdenable(texto){
	//Convertir los string que se sacan de las webs, a un float para que se pueda ordenar.
	if(texto !== undefined){
		return parseFloat(texto.replace("$", "").replace(".","").replace(",",".").replace(/^[a-zA-Z]*$/,""));
	}
}

function pushAJson(param,json){
	if(param.titulo !== undefined){
		param.titulo = param.titulo.replace("\n                                    ", "").replace("\n                                ","");
		json.push({
			"titulo":param.titulo, 
			"precio":param.precio, 
			"url":param.url, 
			"array_categorias": [], 
			"marca": "",
			"imagen": param.imagen,
			"web": param.web
		});
	}
}


//Delta.
exports.parseDelta = function($, jsonProductos, callback){
	
	var param = {};
	
	$("div.item-container").each(function(){
		param.url = $("a.image", this).attr("href");
		param.titulo = $("div.title a", this).attr("title");
		param.precio = toFloatOrdenable($("div.price", this).text());
		param.imagen = $("a.image img",this).attr('src');
		param.web = "Delta";
		console.log(param.imagen);
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"delta");
};

//Whirlpool Store.
exports.parseWhirlpoolStore = function($, jsonProductos, callback){
	
	var param = {};
	
	$("article.box-produto").each(function(){
		param.url = $("a.image", this).attr("href");
		param.titulo = $("a.image", this).attr("title");
		param.precio = toFloatOrdenable($("p.por", this).text());
		param.imagen = $("span.img.backup img",this).attr('src');
		param.web = "Whirlpool Store";
		console.log(param.imagen);
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"whirlpool store");
};

//Electro Next.
exports.parseElectroNext = function($, jsonProductos, callback){
	
	var param = {};
	
	$("li.row.one-promo").each(function(){
		param.url = "https://www.electronextshop.com.ar" + $("a.title", this).attr("href");
		param.titulo = $("a.title", this).attr("title");
		param.precio = toFloatOrdenable($("span.ch-price.price", this).text());
		param.imagen = $("figure a img",this).attr('src');
		param.web = "Electro Next";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"electro next");
};

//Authogar.
exports.parseAuthogar = function($, jsonProductos, callback){
	
	var param = {};
	
	$("li.item").each(function(){
		param.url = $("a.product-image", this).attr("href");
		param.titulo = $("a.product-image", this).attr("title") + " " + $('div.product-manufacturer',this).text();
		param.precio = toFloatOrdenable($("span.price", this).text());
		param.imagen = $("img.defaultImage",this).attr('src');
		param.web = "Authogar";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"authogar");
};

//Beiro Hogar.
exports.parseBeiroHogar = function($, jsonProductos, callback){
	
	var param = {};
	
	$("div.product").each(function(){
		param.url = $("div.thumbnail a", this).attr("href");
		param.titulo = $("div.thumbnail a", this).attr("title");
		param.precio = toFloatOrdenable($("span.product-price", this).text());
		param.imagen = $("img.img-responsive",this).attr('src');
		param.web = "Beiro Hogar";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"beiro hogar");
};

//Bidcom.
exports.parseBidcom = function($, jsonProductos, callback){
	
	var param = {};
	
	$("div.item-listado").each(function(){
		param.url = $("h2.titulo-listado.t2 a", this).attr("href");
		param.titulo = $("h2.titulo-listado.t2 a", this).attr("title");
		param.precio = toFloatOrdenable($("div.precioproducto", this).text());
		param.imagen = "https://www.bidcom.com.ar" + $("img.img-imagen-listado",this).attr('src');
		param.web = "Bidcom";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"bidcom");
};

//Cetrogar.
exports.parseCetrogar = function($, jsonProductos, callback){
	
	var param = {};
	
	$("li.item").each(function(){
		param.url = $("h2.product-name a", this).attr("href");
		param.titulo = $("h2.product-name a", this).attr("title");
		param.precio = toFloatOrdenable($("span.price", this).text());
		param.imagen = $("a.product-image img",this).attr('src');
		param.web = "Cetrogar";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"cetrogar");
};


//Carrefour.
exports.parseCarrefour = function($, jsonProductos, callback){
	
	var param = {};
	
	$("li.item").each(function(){
		param.url = $("h2.product-name a", this).attr("href");
		param.titulo = $("h2.product-name a", this).attr("title");
		param.precio = toFloatOrdenable($("span.price", this).text());
		param.imagen = $("a.product-image img",this).attr('src');
		param.web = "Carrefour";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"carrefour");
};


//Compumundo.
exports.parseCompumundo = function($, jsonProductos, callback){
	
	var param = {};

	$('.gb-product-list').filter(function(){
		// Recorro todos los elementos <div> con class wrapData y saco los datos.
		$("div.gb-list-cluster", this).each(function(){
			param.url = "https://www.compumundo.com.ar" + $("a", this).attr("href");
			param.titulo = $("h3.gb-list-cluster-title", this).text();
			param.precio = toFloatOrdenable($(".gb-list-cluster-prices-current", this).text());
			param.imagen = $("div.gb-list-cluster-picture-inner img",this).attr('src');
			param.web = "Compumundo";
			//Agrego cada json al array.
			pushAJson(param,jsonProductos);
		});
	});
	callback(null,"compumundo");
};


//Celutronic
exports.parseCelutronic = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div#grid-extended div.product-inner").each(function(){
		param.url = $("a.woocommerce-LoopProduct-link.woocommerce-loop-product__link", this).attr("href");
		param.titulo = $("h2.woocommerce-loop-product__title", this).text();
		param.precio = toFloatOrdenableIngles($("span.woocommerce-Price-amount.amount", this).text());
		param.imagen = $("div.product-thumbnail img",this).attr('src');
		param.web = "Celutronic";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"celutronic");
};


//Pardo
exports.parsePardo = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("li.item").each(function(){
		param.url = "https://www.pardo.com.ar" + $("h3 a", this).attr("href");
		param.titulo = $("h3 a", this).text();
		param.precio = toFloatOrdenable($("span.price.display-price", this).text());
		param.imagen = $("a img",this).attr('src');
		param.web = "Pardo";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"pardo");
};


//MercadoLibre
exports.parseMercadolibre = function (data, jsonProductos, callback){
	
	var param = {};
	
	var arrayResultados = data.results;
	
	for(var i=0; i<arrayResultados.length; i++){
		param.url = arrayResultados[i].permalink;
		param.titulo = arrayResultados[i].title;
		param.precio = arrayResultados[i].price;
		var thumbnail = arrayResultados[i].thumbnail;
		param.imagen = thumbnail.replaceAt((thumbnail.length - 5), "F");
		param.web = "Mercado Libre";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	}

	callback(null,"mercadolibre");
};


//Tiomusa
exports.parseTiomusa = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div.col-lg-4.col-md-6.col-sm-6.col-xs-12").each(function(){
		param.url = "http://www.tiomusa.com.ar" + $("a.titulo", this).attr("href");
		param.titulo = $("a.titulo", this).attr("title");
		param.precio = toFloatOrdenable($("div.precio-online strong", this).text());
		param.imagen = $("a.img img",this).attr('src');
		param.web = "Tio Musa";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"tiomusa");
};


//Ansila
exports.parseAnsila = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("li.row.one-promo").each(function(){
		param.url = "https://www.hiperansila.com.ar" + $("a.title", this).attr("href");
		param.titulo = $("a.title", this).text();
		param.precio = toFloatOrdenable($("span.ch-price.price", this).text());
		param.imagen = $("figure a img",this).attr('src');
		param.web = "Ansila";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"ansila");
};


//Musimundo
exports.parseMusimundo = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("article.product").each(function(){
		param.url = $("a.name.productClicked", this).attr("href");
		param.titulo = $("a.name.productClicked", this).text();
		param.precio = toFloatOrdenable($("span.price.online", this).text());
		param.imagen = $("a.img.productClicked img",this).attr('src');
		param.web = "Musimundo";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"musimundo");
};


//Electropuntonet
exports.parseElectropuntonet = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div.one-product").each(function(){
		param.url = "http://www.electropuntonet.com" + $("div.col-md-6.one-prod-img a", this).attr("href");
		param.titulo = $("h2.prod-title", this).text();
		param.precio = toFloatOrdenable($("span.numero-cuota.red", this).text());
		param.imagen = "http://www.electropuntonet.com" + $("img.main-prod",this).attr('src');
		param.web = "Electropuntonet";
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"electropuntonet");
};


//Falabella
exports.parseFalabella = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div.fb-pod-group__item.fb-pod-group__item--product").each(function(){
		param.url = "https://www.falabella.com.ar" + $("h4.fb-responsive-hdng-5.fb-pod__product-title a", this).attr("href");
		param.titulo = $("h4.fb-responsive-hdng-5.fb-pod__product-title a", this).attr("title");
		param.precio = toFloatOrdenable($("p.fb-pod__product-price", this).text());
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	callback(null,"falabella");
};


//Linio
exports.parseLinio = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div.catalogue-product.row").each(function(){
		param.url = "https://www.linio.com.ar" + $("a.title-section", this).attr("href");
		param.titulo = $("a.title-section", this).attr("title");
		param.precio = toFloatOrdenable($(".price-secondary", this).text());
		param.imagen = "https://" + $("img",this).attr('data-lazy');
		param.web = "Linio";
		//Agrego cada json al array.
		if(param.precio){
			pushAJson(param,jsonProductos);
		}
	});
	
	callback(null,"linio");
	
};


//Maxihogar
exports.parseMaxihogar = function ($, jsonProductos, callback){
	
	var param = {};

	// Recorro todos los elementos <div> con class product y saco los datos.

	$("div.product").each(function(){
		param.url = $("a.productName", this).attr("href");
		param.titulo = $("a.productName", this).text();
		param.precio = toFloatOrdenable($("div.bestPrice", this).text());
		param.imagen = $("img",this).attr('src');
		param.web = "Maxihogar";
		
		//Agrego cada json al array.
		pushAJson(param,jsonProductos);
	});
	
	callback(null,"maxihogar");
	
};


//Frávega.
exports.parseFravega = function ($, jsonProductos, callback){
	
	var param = {};

	$('.main').filter(function(){
		// Recorro todos los elementos <div> con class wrapData y saco los datos.
		$("div.wrapData", this).each(function(){
			param.url = $("h3 a", this).attr("href");
			param.titulo = $("h3 a", this).text();
			param.precio = toFloatOrdenable($("em.BestPrice", this).text());
			param.imagen = $("div.image a[href='"+ param.url +"'] img").attr('src');
			param.web = "Frávega";
			//Agrego cada json al array.
			pushAJson(param,jsonProductos);
		});
	});
	callback(null,"fravega");
	
};


//Garbarino.
exports.parseGarbarino = function($, jsonProductos, callback){
	
	var param = {};

	$('.gb-product-list').filter(function(){
		// Recorro todos los elementos <div> con class wrapData y saco los datos.
		$("div.gb-list-cluster", this).each(function(){
			param.url = "https://www.garbarino.com" + $("a", this).attr("href");
			param.titulo = $("h3.gb-list-cluster-title", this).text();
			param.precio = toFloatOrdenable($(".gb-list-cluster-prices-current", this).text());
			param.imagen = 'https://' + $("div.gb-list-cluster-picture img", this).attr('src');
			param.web = "Garbarino";
			//Agrego cada json al array.
			pushAJson(param,jsonProductos);
		});
	});
	callback(null,"garbarino");
};
