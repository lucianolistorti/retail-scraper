var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/dbProyectoWeb");

var busqueda_schema = new Schema({
	title: String,
	id_category: String,
	//Cantidad de busquedas de este termino.
	searches: Number,
	//Valores contiene el json con toda la info a mostrar en la busqueda (productos, filtros).
	valores: Object
});

busqueda_schema.statics.incrementarSearches = function(tituloBusqueda, callback) {
	  return this.findOne({ title: tituloBusqueda }, function(err, busqueda){
		  busqueda.searches = busqueda.searches + 1;
		  busqueda.save(function(err){
				if(err){
					console.log(String(err));
				}
				else {
					console.log("Se ha incrementado busqueda.");
				}
				callback(err,busqueda);
			});	
	  });
};

var Busqueda = mongoose.model("Busqueda", busqueda_schema);

module.exports.Busqueda = Busqueda;