var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/dbProyectoWeb");

var producto_schema = new Schema({
	name: String,
	categories: [],
	brand: String
});

var Producto = mongoose.model("Producto", producto_schema);

module.exports.Producto = Producto;