var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/dbProyectoWeb");

var sitio_schema = new Schema({
	name: String,
	url: String,
});


var Sitio = mongoose.model("Sitio", sitio_schema);

module.exports.Sitio = Sitio;