var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/dbProyectoWeb");

var category_schema = new Schema({
	//id en Mercado Libre.
	id_ML: String,
	name: String,
	//Array de webs a las que se hace scraping en esta categoria.
	list_of_webs: [],
	//Búsquedas a esta categoría.
	searches: Number,
	list_admited: [],
	list_brands: []
});

category_schema.statics.incrementarSearches = function(idCategoria, callback) {
	  return this.findOne({ id_ML: idCategoria }, function(err, categoria){
		  categoria.searches = categoria.searches + 1;
		  categoria.save(function(err){
				if(err){
					console.log(String(err));
				}
				else {
					console.log("Se ha incrementado categoria.");
				}
				callback(err,categoria);
			});	
	  });
};

var Categoria = mongoose.model("Categoria", category_schema);

module.exports.Categoria = Categoria;