var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.connect("mongodb://localhost/dbProyectoWeb");

var categoryML_schema = new Schema({
	id_ML: String,
	name: String,
	path_from_root: [],
	children_categories: []
});

var CategoriaML = mongoose.model("CategoriaML", categoryML_schema);

module.exports.CategoriaML = CategoriaML;