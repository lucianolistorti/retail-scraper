
//Model.
var Busqueda = require("../models/busqueda").Busqueda;
var Categoria = require("../models/categoria").Categoria;

var searchEngine = require('../search-engine/searchengine-controller');

var async = require('async');

var start = new Date().getTime();

actualizarDatosParalelo();

/*
var string = "hola como va";
var alphabets = string.split(" ");

permute(alphabets, 0, alphabets.length-1);

function swap (alphabets, index1, index2) {
  var temp = alphabets[index1];
  alphabets[index1] = alphabets[index2];
  alphabets[index2] = temp;
  return alphabets;
}

function permute (alphabets, startIndex, endIndex) {
  if (startIndex === endIndex) {
	console.log(alphabets);
  } else {
    var i;
    for (i = startIndex; i <= endIndex; i++) {
      swap(alphabets, startIndex, i);
      permute(alphabets, startIndex + 1, endIndex);
      swap(alphabets, i, startIndex); // backtrack
    }
  }
} */

function hacerBusqueda(params,callback){
		var query = params.busqueda.title;
		var id_categoria = params.busqueda.id_category;
		var products = [];

		function esCategoria(json) { 
			return json.id_ML === id_categoria;
		}
		var listOfWebs = params.categorias.find(esCategoria).list_of_webs;
		
		searchEngine.doQuerySearch(query, listOfWebs, (results) => {
			callback();
			guardarBusqueda(query, results);
		});
}

function guardarBusqueda(busqueda,jsonResultado){
	Busqueda.update({title: jsonResultado.query}, { $set: { valores: jsonResultado, searches: 0 } },  function(err){
		console.log("busqueda guardada.")
	});
}


function recorrerBusquedasParalelo(i,busquedas,categorias,callback){
	
	if(i<busquedas.length){
		var funcionesRecorrerBusquedas = [];
		
		for(var j=i; j<(i+50); j++){
			if(j<busquedas.length){
				var params = {
						busqueda: busquedas[j],
						categorias: categorias
				};
				funcionesRecorrerBusquedas.push(hacerBusqueda.bind(null,params));
			}
			else {
				break;
			}
		}
		
		async.parallel(funcionesRecorrerBusquedas, function(err, results) {
			console.log("FINALIZARON " + (j-i) + " busquedas");
			var actual = new Date().getTime();
			var tiempo = actual - start;
			console.log("Tiempo tardado: " + tiempo);
			i = i+50;
			recorrerBusquedasParalelo(i,busquedas,categorias,callback);
		});
		
	}
	else {
		callback();
	}	
}

function actualizarDatosParalelo(){

	Busqueda.find({}, function(err,busquedas){
		
		console.log("Se recorren " + busquedas.length + " busquedas");

		Categoria.find({}, function(err,categorias){
			
			recorrerBusquedasParalelo(0,busquedas,categorias, () => {
				console.log("FIN DE TODO");
				var actual = new Date().getTime();
				var tiempo = actual - start;
				console.log("Tiempo tardado: " + tiempo);
			});

		});
	});
}