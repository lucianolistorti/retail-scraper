var Categoria = require("./models/categoria").Categoria;
var Busqueda = require("./models/busqueda").Busqueda;
var CategoriaML = require("./models/categoriaML").CategoriaML;
var Sitio = require("./models/sitio").Sitio;
var request = require('request');

var urlBase = "https://api.mercadolibre.com/categories/";

//agregarSitio("cetrogar","https://www.cetrogar.com.ar/catalogsearch/result/?q=");
//agregarListaACategoria("MLA1051",["carrefour","cetrogar","pardo","celutronic","compumundo","mercadolibre","tiomusa","ansila","musimundo","electropuntonet","falabella","linio","garbarino","fravega","maxihogar"]);
//agregarListaACategoria("MLA1051",["cetrogar"]);
/*agregarListaACategoria("MLA1051",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA1000",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA1743",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA5726",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA1574",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA1648",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
agregarListaACategoria("MLA1540",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
*/  

//agregarListaACategoria("MLA5726",["carrefour","cetrogar","pardo","celutronic","compumundo","tiomusa","ansila","musimundo","electropuntonet","linio","garbarino","fravega","maxihogar"]);
//agregarSitio("delta","https://delta.com.ar/Search?searchText=");


/*
var url = "https://www.ribeiro.com.ar/browse/?_dyncharset=UTF-8&Dy=1&Nty=1&minAutoSuggestInputLength=3&autoSuggestServiceUrl=%2Fassembler%3FassemblerContentCollection%3D%5B%2Fcontent%2FShared%2FAuto-Suggest+Panels%5D%26format%3Djson&searchUrl=&containerClass=search_rubricator&defaultImage=%2Fimages%2Fno_image_auto_suggest.png&rightNowEnabled=false&siteScope=ok&_D%3AsiteScope=+&Ntt=heladeras&search=&_D%3Asearch=+&_DARGS=%2Fcartridges%2FSearchBoxAutoSuggest%2FSearchBoxAutoSuggest.jsp";
//url = url.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
request.get({	
	url: url,
	json: true,
	headers: {'User-Agent': 'request'}
}, (err, res, data) => {
	var arrayCategorias = [];
	if (err) {
		console.log('Error:', err);
	} else if (res.statusCode !== 200) {
		console.log('Status:', res.statusCode);
	} else {
		
		//var arrayCategorias = data.path_from_root
		
		console.log(data);
	}
});
*/

requestSearch("Auriculares Sony MDREX15LPPIZUC Rosa", (data) => {

	var url = "https://api.mercadolibre.com/categories/" + data.results[0].category_id;

	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			console.log('Status:', res.statusCode);
		} else {
			console.log(JSON.stringify(data));
		}
	});
})

function requestSearch(busqueda,callback){
	
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	
	var url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busquedaURL;
	
	request.get({
		url: url,
		timeout: 10000,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestSearch(busqueda,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback("");
			console.log('Status:', res.statusCode);
		} else {
			callback(data);
		}
	});
}

//recorrerYGuardarMarcas("https://api.mercadolibre.com/categories/MLA1000", "MLA1000");
//recorrerYGuardarAdmitidos("https://api.mercadolibre.com/categories/MLA1648", "MLA1648");



function agregarAdicionalesElectronicaAudioVideo(){
	Categoria.findOne({id_ML: "MLA1000"},  function(err,categoria){
		var arrayAdicionales = [];

		//Auriculares.
			//Clip de Oreja.
		categoria.list_admited.push("MLA44389");
			//Con Vincha
		categoria.list_admited.push("MLA10585");
			//In Ear
		categoria.list_admited.push("MLA10558");
		
		//Televisores.
			//Smart TV.
		categoria.list_admited.push("MLA123858");
			//LED.
		categoria.list_admited.push("MLA81531");
			//4k.
		categoria.list_admited.push("MLA321526");
			//Pulgadas.
		categoria.list_admited.push("MLA389546");
		categoria.list_admited.push("MLA321527");
		categoria.list_admited.push("MLA323020");
		categoria.list_admited.push("MLA323039");
		categoria.list_admited.push("MLA391082");
		categoria.list_admited.push("MLA389565");
		categoria.list_admited.push("MLA389584");
		categoria.list_admited.push("MLA390293");
		categoria.list_admited.push("MLA390312");
		categoria.list_admited.push("MLA390274");
		categoria.list_admited.push("MLA324064");
		categoria.list_admited.push("MLA391521");
		categoria.list_admited.push("MLA391289");
		categoria.list_admited.push("MLA410309");
		categoria.list_admited.push("MLA410493");
		categoria.list_admited.push("MLA389368");
		categoria.list_admited.push("MLA390122");
		categoria.list_admited.push("MLA389622");
		categoria.list_admited.push("MLA391493");
		categoria.list_admited.push("MLA2829");
		categoria.list_admited.push("MLA323252");
		categoria.list_admited.push("MLA323271");
		categoria.list_admited.push("MLA391087");
		categoria.list_admited.push("MLA389641");
		categoria.list_admited.push("MLA389660");
		categoria.list_admited.push("MLA390388");
		categoria.list_admited.push("MLA390407");
		categoria.list_admited.push("MLA390426");
		categoria.list_admited.push("MLA324102");
		categoria.list_admited.push("MLA391526");
		categoria.list_admited.push("MLA391294");
		categoria.list_admited.push("MLA410314");
		categoria.list_admited.push("MLA410498");
		categoria.list_admited.push("MLA389367");
		categoria.list_admited.push("MLA389487");
		categoria.list_admited.push("MLA124893");
		categoria.list_admited.push("MLA322982");
		categoria.list_admited.push("MLA323001");
		categoria.list_admited.push("MLA391080");
		categoria.list_admited.push("MLA389468");
		categoria.list_admited.push("MLA389506");
		categoria.list_admited.push("MLA390236");
		categoria.list_admited.push("MLA390255");
		categoria.list_admited.push("MLA390217");
		categoria.list_admited.push("MLA324026");
		categoria.list_admited.push("MLA391520");
		categoria.list_admited.push("MLA391287");
		categoria.list_admited.push("MLA410308");
		categoria.list_admited.push("MLA410492");
		categoria.list_admited.push("MLA389369");
		categoria.list_admited.push("MLA389772");
		categoria.list_admited.push("MLA14874");
		categoria.list_admited.push("MLA323328");
		categoria.list_admited.push("MLA323347");
		categoria.list_admited.push("MLA391084");
		categoria.list_admited.push("MLA389791");
		categoria.list_admited.push("MLA389810");
		categoria.list_admited.push("MLA390464");
		categoria.list_admited.push("MLA390483");
		categoria.list_admited.push("MLA390445");
		categoria.list_admited.push("MLA324178");
		categoria.list_admited.push("MLA391523");
		categoria.list_admited.push("MLA391291");
		categoria.list_admited.push("MLA410311");
		categoria.list_admited.push("MLA410495");
		categoria.list_admited.push("MLA389365");
		categoria.list_admited.push("MLA389697");
		categoria.list_admited.push("MLA321623");
		categoria.list_admited.push("MLA323290");
		categoria.list_admited.push("MLA323309");
		categoria.list_admited.push("MLA391083");
		categoria.list_admited.push("MLA389716");
		categoria.list_admited.push("MLA389735");
		categoria.list_admited.push("MLA390331");
		categoria.list_admited.push("MLA390350");
		categoria.list_admited.push("MLA390369");
		categoria.list_admited.push("MLA324140");
		categoria.list_admited.push("MLA391522");
		categoria.list_admited.push("MLA391290");
		categoria.list_admited.push("MLA410310");
		categoria.list_admited.push("MLA410494");
		categoria.list_admited.push("MLA389366");
		categoria.list_admited.push("MLA389884");
		categoria.list_admited.push("MLA11899");
		categoria.list_admited.push("MLA323560");
		categoria.list_admited.push("MLA323579");
		categoria.list_admited.push("MLA391086");
		categoria.list_admited.push("MLA389903");
		categoria.list_admited.push("MLA389922");
		categoria.list_admited.push("MLA390559");
		categoria.list_admited.push("MLA390578");
		categoria.list_admited.push("MLA390597");
		categoria.list_admited.push("MLA324292");
		categoria.list_admited.push("MLA391525");
		categoria.list_admited.push("MLA391293");
		categoria.list_admited.push("MLA410313");
		categoria.list_admited.push("MLA410497");
		categoria.list_admited.push("MLA389362");
		categoria.list_admited.push("MLA390034");
		categoria.list_admited.push("MLA81536");
		categoria.list_admited.push("MLA323404");
		categoria.list_admited.push("MLA323423");
		categoria.list_admited.push("MLA391085");
		categoria.list_admited.push("MLA390053");
		categoria.list_admited.push("MLA390072");
		categoria.list_admited.push("MLA390521");
		categoria.list_admited.push("MLA390540");
		categoria.list_admited.push("MLA390502");
		categoria.list_admited.push("MLA324216");
		categoria.list_admited.push("MLA391524");
		categoria.list_admited.push("MLA391292");
		categoria.list_admited.push("MLA410312");
		categoria.list_admited.push("MLA410496");
		categoria.list_admited.push("MLA389364");
		categoria.list_admited.push("MLA389397");
		categoria.list_admited.push("MLA123863");
		categoria.list_admited.push("MLA322868");
		categoria.list_admited.push("MLA322887");
		categoria.list_admited.push("MLA391079");
		categoria.list_admited.push("MLA389430");
		categoria.list_admited.push("MLA389449");
		categoria.list_admited.push("MLA390179");
		categoria.list_admited.push("MLA390198");
		categoria.list_admited.push("MLA390160");
		categoria.list_admited.push("MLA323988");
		categoria.list_admited.push("MLA391519");
		categoria.list_admited.push("MLA391286");
		categoria.list_admited.push("MLA410307");
		categoria.list_admited.push("MLA410491");
		categoria.list_admited.push("MLA389370");


		agregarListaAdmitidosACategoria("MLA1000",categoria.list_admited);
		console.log(categoria.list_admited.length);
		console.log(categoria.list_admited);
	});
}

function agregarAdicionalesElectrodomesticos(){

	Categoria.findOne({id_ML: "MLA5726"},  function(err,categoria){
		var arrayAdicionales = [];
		console.log(categoria.list_admited.length);
		
		//Lavarropas Automático o Semi automático.
		categoria.list_admited.push("MLA10077");
		categoria.list_admited.push("MLA10062");
		
		//Lavarropas: carga superior o frontal (automático y semi automático).
		categoria.list_admited.push("MLA10282");
		categoria.list_admited.push("MLA10287");
		categoria.list_admited.push("MLA18375");
		categoria.list_admited.push("MLA18376");
		
		//Heladeras Frio Directo, Neo Frost, No frost.
		categoria.list_admited.push("MLA398765");
		categoria.list_admited.push("MLA398767");
		categoria.list_admited.push("MLA398766");
		categoria.list_admited.push("MLA398756");
		categoria.list_admited.push("MLA398758");
		categoria.list_admited.push("MLA398757");
		categoria.list_admited.push("MLA398768");
		categoria.list_admited.push("MLA398770");
		categoria.list_admited.push("MLA398769");
		categoria.list_admited.push("MLA398744");
		categoria.list_admited.push("MLA398746");
		categoria.list_admited.push("MLA398745");
		categoria.list_admited.push("MLA398750");
		categoria.list_admited.push("MLA398752");
		categoria.list_admited.push("MLA398751");
		categoria.list_admited.push("MLA398759");
		categoria.list_admited.push("MLA398761");
		categoria.list_admited.push("MLA398760");
		categoria.list_admited.push("MLA398753");
		categoria.list_admited.push("MLA398755");
		categoria.list_admited.push("MLA398754");
		categoria.list_admited.push("MLA398762");
		categoria.list_admited.push("MLA398764");
		categoria.list_admited.push("MLA398763");
		categoria.list_admited.push("MLA398741");
		categoria.list_admited.push("MLA398743");
		categoria.list_admited.push("MLA398742");
		categoria.list_admited.push("MLA398747");
		categoria.list_admited.push("MLA398749");
		categoria.list_admited.push("MLA398748");
		categoria.list_admited.push("MLA398795");
		categoria.list_admited.push("MLA398797");
		categoria.list_admited.push("MLA398796");
		categoria.list_admited.push("MLA398780");
		categoria.list_admited.push("MLA398782");
		categoria.list_admited.push("MLA398781");
		categoria.list_admited.push("MLA398771");
		categoria.list_admited.push("MLA398773");
		categoria.list_admited.push("MLA398772");
		categoria.list_admited.push("MLA411091");
		categoria.list_admited.push("MLA411092");
		categoria.list_admited.push("MLA411093");
		categoria.list_admited.push("MLA398801");
		categoria.list_admited.push("MLA398803");
		categoria.list_admited.push("MLA398802");
		categoria.list_admited.push("MLA398777");
		categoria.list_admited.push("MLA398779");
		categoria.list_admited.push("MLA398778");
		categoria.list_admited.push("MLA398792");
		categoria.list_admited.push("MLA398794");
		categoria.list_admited.push("MLA398793");
		categoria.list_admited.push("MLA398810");
		categoria.list_admited.push("MLA398812");
		categoria.list_admited.push("MLA398811");
		categoria.list_admited.push("MLA398813");
		categoria.list_admited.push("MLA398815");
		categoria.list_admited.push("MLA398814");
		categoria.list_admited.push("MLA398774");
		categoria.list_admited.push("MLA398776");
		categoria.list_admited.push("MLA398775");
		categoria.list_admited.push("MLA398816");
		categoria.list_admited.push("MLA398818");
		categoria.list_admited.push("MLA398817");
		categoria.list_admited.push("MLA398789");
		categoria.list_admited.push("MLA398791");
		categoria.list_admited.push("MLA398790");
		categoria.list_admited.push("MLA398798");
		categoria.list_admited.push("MLA398800");
		categoria.list_admited.push("MLA398799");
		categoria.list_admited.push("MLA398783");
		categoria.list_admited.push("MLA398785");
		categoria.list_admited.push("MLA398784");
		categoria.list_admited.push("MLA398804");
		categoria.list_admited.push("MLA398806");
		categoria.list_admited.push("MLA398805");
		categoria.list_admited.push("MLA398786");
		categoria.list_admited.push("MLA398788");
		categoria.list_admited.push("MLA398787");
		categoria.list_admited.push("MLA398837");
		categoria.list_admited.push("MLA398839");
		categoria.list_admited.push("MLA398838");
		categoria.list_admited.push("MLA398819");
		categoria.list_admited.push("MLA398821");
		categoria.list_admited.push("MLA398820");
		categoria.list_admited.push("MLA398840");
		categoria.list_admited.push("MLA398842");
		categoria.list_admited.push("MLA398841");
		categoria.list_admited.push("MLA398828");
		categoria.list_admited.push("MLA398830");
		categoria.list_admited.push("MLA398829");
		categoria.list_admited.push("MLA398849");
		categoria.list_admited.push("MLA398851");
		categoria.list_admited.push("MLA398850");
		categoria.list_admited.push("MLA398831");
		categoria.list_admited.push("MLA398833");
		categoria.list_admited.push("MLA398832");
		categoria.list_admited.push("MLA398825");
		categoria.list_admited.push("MLA398827");
		categoria.list_admited.push("MLA398826");
		categoria.list_admited.push("MLA398852");
		categoria.list_admited.push("MLA398854");
		categoria.list_admited.push("MLA398853");
		categoria.list_admited.push("MLA398846");
		categoria.list_admited.push("MLA398848");
		categoria.list_admited.push("MLA398847");
		categoria.list_admited.push("MLA398807");
		categoria.list_admited.push("MLA398809");
		categoria.list_admited.push("MLA398808");
		categoria.list_admited.push("MLA398834");
		categoria.list_admited.push("MLA398836");
		categoria.list_admited.push("MLA398835");
		categoria.list_admited.push("MLA398843");
		categoria.list_admited.push("MLA398845");
		categoria.list_admited.push("MLA398844");
		categoria.list_admited.push("MLA398855");
		categoria.list_admited.push("MLA398857");
		categoria.list_admited.push("MLA398856");
		categoria.list_admited.push("MLA398822");
		categoria.list_admited.push("MLA398824");
		categoria.list_admited.push("MLA398823");

		agregarListaAdmitidosACategoria("MLA5726",categoria.list_admited);
		console.log(categoria.list_admited.length);
		console.log(categoria.list_admited);
	});
}

function agregarAdicionalesCelularesYSmartphones(){
	Categoria.findOne({id_ML: "MLA1051"},  function(err,categoria){
		var arrayAdicionales = [];
		
		
		
		agregarListaAdmitidosACategoria("MLA1051",categoria.list_admited);
		console.log(categoria.list_admited.length);
		console.log(categoria.list_admited);
	});
}

function crearCategorias(){
	
	request.get({
	    url: "https://api.mercadolibre.com/sites/MLA/categories",
	    json: true,
	    headers: {'User-Agent': 'request'}
	  }, (err, res, data) => {
	    if (err) {
	      console.log('Error:', err);
	    } else if (res.statusCode !== 200) {
	      console.log('Status:', res.statusCode);
	    } else {
	      // data is already parsed as JSON:
	    		var id_ML, name;
	    		for(var i=0; i<data.length; i++){
	    			//Recorro las categorias de ML y las guardo en mi base de datos.
	    			var categoria = new Categoria({
	    				id_ML: data[i].id,
	    				name: data[i].name,
	    				list_of_webs: [],
	    				searches: 0
	    			});
	    			categoria.save(function(err){
	    				if(err){
	    					console.log(String(err));
	    				}
	    				else {
	    					console.log("datos guardados");
	    				}
	    			});
	    		}
	    }
	});
}

function agregarListaMarcasACategoria(idCategoria, array){

	//Agregar {multi: true} para modificar mas de 1.
	switch(idCategoria){
		//Electrónica Audio y Video.
		case "MLA1000":
			array.push("MLA10558","MLA10585","MLA10573");
			break;
	}
	
	Categoria.update({id_ML: idCategoria}, { $set: { list_brands: array } },  function(err){
		if(err){
			console.log(String(err));
		}
		else {
			console.log("datos guardados");
		}
	});
}

function agregarListaAdmitidosACategoria(idCategoria, array){

	//Agregar {multi: true} para modificar mas de 1.
	Categoria.update({id_ML: idCategoria}, { $set: { list_admited: array } },  function(err){
		if(err){
			console.log(String(err));
		}
		else {
			console.log("datos guardados");
		}
	});
}

function guardarCategoriaML(data, categoriaRoot){
	
	console.log(data.id);
	console.log(data.name);
	console.log(categoriaRoot);

	var categoriaML = new CategoriaML({
		id_ML: data.id,
		name: data.name,
		path_from_root: data.path_from_root,
		children_categories: data.children_categories
	});
	
	categoriaML.save(function(err){
		if(err){
			console.log(String(err));
		}
		else {
			console.log("datos guardados");
		}
	});
}

var arrayMarcas = [];

function recorrerYGuardarMarcas(url, categoriaRoot, callback){

	request.get({
		url: url,
		json: true,
		timeout: 120000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			//console.log('Error:', err);
			console.log("Reintentando " + url)
			recorrerYGuardarMarcas(url,categoriaRoot);
		} else if (res.statusCode !== 200) {
			console.log('Status:', res.statusCode);
		} else {
			
			var array = data.children_categories;
			var id_ML, name, category;
			
			
			
			/*
			if( data.name.includes("Otras marcas") ){
				var index = data.path_from_root.length - 2;
				console.log('case "'+ data.path_from_root[index] + '":') ;
				console.log('esPadreMarca = true;');
				console.log('break;');

			}*/
			
			function esPadreMarca(categoria) { 
				
			    return ((categoria.name === 'Otras Marcas') || (categoria.name === 'Otras marcas'));
			}
			
			//MLA10558 es un caso especial, no tiene "otras marcas", tiene "otros".
			if( (array.find(esPadreMarca) !== undefined)){
				arrayMarcas.push(data.id);
				agregarListaMarcasACategoria(categoriaRoot, arrayMarcas);
				//console.log('case "'+ data.id + '":') ;
				//console.log('esPadreMarca = true;');
				//console.log('break;');
			}
			else {
				if(array.length !== 0){
					//No es hoja. Seguir recorriendo.
					for(var i=0; i<array.length; i++){
						//Recorro las categorias de ML y las guardo en mi base de datos.	
						recorrerYGuardarMarcas(urlBase+array[i].id,categoriaRoot);		
					}
				}
			}	
		}
	});
}

var arrayAdmitidos = [];

function recorrerYGuardarAdmitidos(url, categoriaRoot){

	request.get({
		url: url,
		json: true,
		timeout: 120000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			//console.log('Error:', err);
			console.log("Reintentando " + url)
			recorrerYGuardarAdmitidos(url,categoriaRoot);
		} else if (res.statusCode !== 200) {
			console.log('Status:', res.statusCode);
		} else {
			
			var array = data.children_categories;
			var id_ML, name, category;
			
			if(data.name.includes("Otras") || data.name.includes("Otros") || data.name.includes("otros") || data.name.includes("otras")){
				arrayAdmitidos.push(data.id);
				agregarListaAdmitidosACategoria(categoriaRoot, arrayAdmitidos);
				//console.log('case "'+ data.id + '":') ;
				//console.log('admite = true;');
				//console.log('break;');
			}
			
			if(array.length !== 0){
				//No es hoja. Seguir recorriendo.
				for(var i=0; i<array.length; i++){
					//Recorro las categorias de ML y las guardo en mi base de datos.	
					recorrerYGuardarAdmitidos(urlBase+array[i].id,categoriaRoot);		
				}
				
			}
		}
	});
}

function agregarSitio(nombre,url){
	var sitio = new Sitio({
		name: nombre,
		url: url
	});
	
	sitio.save(function(err){
		if(err){
			console.log(String(err));
		}
		else {
			console.log("datos guardados");
		}
	});
}

//Para guardar un array de funciones.
function agregarListaACategoria(idCategoria, array){

	//Agregar {multi: true} para modificar mas de 1.
	Categoria.update({id_ML: idCategoria}, { $set: { list_of_webs: array } },  function(err){
	});
}
//EJEMPLO: agregarListaACategoria("MLA1051",["fravega","garbarino"]);
