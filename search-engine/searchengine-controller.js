var async = require('async');

var webFunctionsFactory = require('./scraper/webfunctions');

var filterResultsByCategory = require("./filterer/product-filterer").filterResultsByCategory;


function doQuerySearch(searchQuery, listOfWebs, callback){
	
	//Internally, all the queries are in lower case.
	searchQuery = searchQuery.toLowerCase();
	
	searchQueryOnWebs(0, listOfWebs, searchQuery, [], [], (results) => {
		callback(results);
	});
}

function searchQueryOnWebs(i, listOfWebs, query, products, webFunctions, callback){

	var param = {
			busqueda : query,
			jsonProductos: products
	};

	if(i < listOfWebs.length){
		webFunctionsFactory.getWebFunction(listOfWebs[i], (webFunction) => {
			webFunctions.push(webFunction.bind(null,param));
			i=i+1;
			searchQueryOnWebs(i, listOfWebs, query, products, webFunctions, callback);
		});
	}
	else {
		//Terminé de recorrer las webs de la busqueda, ejecuto las funciones y llamo a callback.
		async.parallel(webFunctions, function(err, results) {
			console.log("Sin filtrar: " + JSON.stringify(products));
			filterResultsByCategory(query, products, (jsonResults) => {
				jsonResults.products.sort(function(a,b){
					return a.precio - b.precio;
				});
				callback(jsonResults);
				console.log(JSON.stringify(jsonResults));
				console.log("FIN "+ query);
			});
		});
	}
}

module.exports.doQuerySearch = doQuerySearch;