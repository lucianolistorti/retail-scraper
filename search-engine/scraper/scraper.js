/**
 * http://usejsdoc.org/
 */

var request = require('request');
var cheerio = require('cheerio');

function doScraping(jsonProductos, doParsing, url, busqueda, callback){
	//Abstraccion general de un scraping.
	//doParsing hace el algoritmo de parseo de cada web particular.

	//Se hace uso de 'request' para obtener el html
	request({ url: url, timeout: 10000}, function(error, response, html){

		if(!error){

			console.log("request " + url + " --- " + busqueda);

			//Se hace uso de cheerio sobre el html para tener funcionalidad jQuery y recorrer el DOM.
			var $ = cheerio.load(html);
			
			doParsing($, jsonProductos, callback);		
		}
		
		else {
			console.log(error);
			console.log("Reintentando "+ url);
			doScraping(jsonProductos, doParsing, url, busqueda, callback);
		}
	});
};


exports.doScraping = doScraping;

exports.doAPIScraping = function (jsonProductos, doParsing, url, callback){
	//Hace el request y obtiene la api.
	//doParsing hace el algoritmo de parseo de cada web particular.
	console.log(url);

	//Se hace uso de 'request' para obtener el json.
	request.get({
	    url: url,
	    json: true,
	    headers: {'User-Agent': 'request'}
	  }, (err, res, data) => {
	    if (err) {
	      console.log('Error:', err);
	    } else if (res.statusCode !== 200) {
	      console.log('Status:', res.statusCode);
	    } else {
	      // data is already parsed as JSON:
	    		console.log("request " + url);
			
			doParsing(data, jsonProductos, callback);
	    }
	});
};

function doPageScraping (jsonProductos, doParsing, urlBase, url, i, callback){
	//Abstraccion general de un scraping.
	//doParsing hace el algoritmo de parseo de cada web particular.

	//Se hace uso de 'request' para obtener el html

	request(url, function(error, response, html){

		if((!error) && (response.request.uri.href === url)){

			console.log("request " + url);

			//Se hace uso de cheerio sobre el html para tener funcionalidad jQuery y recorrer el DOM.
			var $ = cheerio.load(html);
			
			doParsing($, jsonProductos, callback);
			
			//Próxima página.
			i++;
			url = urlBase + "&page=" + i;
			doPageScraping(jsonProductos, doParsing, urlBase, url, i, callback);
		}
		else
			callback(null,"falabella");
	});
}

//Phantom "simula" un navegador. Sirve para las webs que cargan contenido en el cliente.
function doPageScrapingPhantom (jsonProductos, doParsing, urlBase, url, i, callback){	

	var phantom = require('phantom');

	phantom.create().then(function(ph) {
		ph.createPage().then(function(page) {
			//Cancela request a recursos de páginas exteras para agilizarlo.
			page.property('onResourceRequested', function(requestData, request){
				if (requestData.url.indexOf("https://www.falabella.com.ar/") != 0) {
			        request.abort();
			    }
			});
			page.open(url).then(function(status) {
				console.log(status);
				page.property('url').then(function(urlFinal) {
					if(urlFinal === url){
						page.property('content').then(function(content) {
							console.log("request " + url);
							//Se hace uso de cheerio sobre el html para tener funcionalidad jQuery y recorrer el DOM.
							var $ = cheerio.load(content);
							doParsing($, jsonProductos, callback);

							//Próxima página.
							i++;
							url = urlBase + "&page=" + i;
							doPageScrapingPhantom(jsonProductos, doParsing, urlBase, url, i, callback);

							page.close();
							ph.exit();
						});
					}
					else
						callback(null,"falabella");
				});

			});
		});
	});
}

//Phantom "simula" un navegador. Sirve para las webs que cargan contenido en el cliente.
function doScrapingPhantom(jsonProductos, doParsing, url, urlBase, callback){	

	var phantom = require('phantom');

	phantom.create().then(function(ph) {
		ph.createPage().then(function(page) {
			//Cancela request a recursos de páginas exteras para agilizarlo.
			page.property('onResourceRequested', function(requestData, request){
				if (requestData.url.indexOf(urlBase) != 0) {
			        request.abort();
			    }
			});
			page.setting('resourceTimeout',1000);
			page.open(url).then(function(status) {
				console.log("entra open")
				page.property('content').then(function(content) {
					console.log("request " + url);
					//Se hace uso de cheerio sobre el html para tener funcionalidad jQuery y recorrer el DOM.
					var $ = cheerio.load(content);
					doParsing($, jsonProductos, callback);
					page.close();
					ph.exit();
				});
			});
		});
	});
}

exports.doScrapingPhantom = doScrapingPhantom;

exports.doPageScrapingPhantom = doPageScrapingPhantom;

exports.doPageScraping = doPageScraping;