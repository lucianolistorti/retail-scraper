var categoryPredictor = require('./category-predictor');

var Categoria = require("../../models/categoria").Categoria;

var async = require('async');
var request = require('request');

function isContained(i,arrayAdmitidos,producto,arrayBusqueda,arrayProducto,filtrosDisponibles,callback){

	console.log("arrayBusqueda: " + arrayBusqueda.length + " " + "arrayProducto: " + arrayProducto.length + " " + producto);
	if(i<arrayBusqueda.length){
		
    	    	if( arrayBusqueda[i].id !== arrayProducto[i].id ){
			//Si entra ya no esta contenido.

			//console.log(arrayBusqueda[i].name +"---"+ arrayProducto[i].name );
			//console.log(arrayBusqueda[i].prediction_probability +"---"+ arrayProducto[i].prediction_probability );
			if(arrayAdmitidos.includes(arrayBusqueda[i].id)){
				//Son "parecidos", puede incluirse.
				//console.log("Parecidos: " + arrayBusqueda[i].name +"---"+ arrayProducto[i].name );
				//console.log(arrayBusqueda[i].prediction_probability +"---"+ arrayProducto[i].prediction_probability );
				callback(true);
			}
			else {
				//Verificar si es un filtro disponible.
				var value = false;
				//Recorro filtros.
				for(var j=0; j<filtrosDisponibles.length; j++){
					if(value === false){
						//Recorro nombres de categorías del filtro.
						for(var k=0; k<filtrosDisponibles[j].valores.length; k++){
							if(arrayProducto[i].name === filtrosDisponibles[j].valores[k].name){
								value = true;
								break;
							}
						}
					}
					else {
						break;
					}
				}
				if(value){
					callback(true);
				}
				else {
					callback(false);
				}
			}
		}
		else {
			isContained(i+1,arrayAdmitidos,producto,arrayBusqueda,arrayProducto,filtrosDisponibles,callback);
		}
	}
	else {
		//Termino de recorrer, esta contenido.
		callback(true);
	}
}

function applyFilters(filtros, categoriasProducto, callback){
	var result = true;
	for(var i=0; i<filtros.length; i++){
		if(result){
			for(var j=0; j<categoriasProducto.length; j++){
				if(categoriasProducto[j].name === filtros[i].valores[0].name){
					result = true;
					break;
				}
				else {
					result = false;
				}
			}
		}
		else {
			break;
		}
	}
	callback(result);
}

function getBrand(i,categories, parentBrands, callback){
	if(i<parentBrands.length){
		if(parentBrands.includes(categories[i].id)){
			//Is cointaned in parent brands array.
			try {
				var brand = categories[i+1].name;
				callback(brand);
			}
			catch (err) {
				console.log("Brand category could not be found from parent category "+ categories[i].id + ": " + err);
				callback("");
			}
			
		}
		else {
			getBrand(i+1,categories,parentBrands,callback);
		}
	}
	else {
		callback("");
	}
}

function evaluateProduct(param, callback){
	categoryPredictor.predictProductCategory(param.producto.titulo, (cbData, err) => {
		if(!err){
			var cbCategories = cbData.categories;
			
			param.producto.marca = cbData.brand;
			param.producto.array_categorias = cbCategories;
			
			console.log("cbCategories de " + param.producto.titulo + ": " + cbCategories);
			
			Categoria.findOne({ id_ML: param.arrayPathBusqueda[0].id }, (err, cbCategory) => {
				
				if(err){
					callback();
					console.log("Error mongoDB search: "+ err);
				}
				else{
					var availableFilters = param.filtros_disponibles;
					
					isContained(0, cbCategory.list_admited, param.producto.titulo, param.arrayPathBusqueda, cbCategories, availableFilters, (value) => {
						if(value === true){
							applyFilters(param.filtros, cbCategories, (result) => {
								if(result === true){
									if(param.producto.array_categorias === ""){
										//There is no filter with "BRAND" id. Search brand in categories.
										getBrand(1, param.producto.array_categorias, cbCategory.list_brands, (marca) => {
											param.producto.marca = marca;
											param.jsonResultados.products.push(param.producto);
											callback();
										});	
									}
									else {
										param.jsonResultados.products.push(param.producto);
										callback();
									}
									
								}
								else {
									console.log("La categoría es:" + JSON.stringify(param.producto.array_categorias));
									console.log("Se descartó "+ param.producto.titulo);
									callback();
								}
							});	
						}
						else {
							console.log("La categoría es:" + JSON.stringify(param.producto.array_categorias));
							console.log("Se descartó "+ param.producto.titulo);
							callback();
						}
					});
				}
			});
		}
		else {	
			console.log("Error, se descarto "+ param.producto.titulo + " " + err);
			callback();
		}
	});
}

function obtenerFiltros(busqueda,callback){
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	var url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busqueda;

	var filtros = [];
	var filtrosDisponibles = [];
	
	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		var arrayCategorias = [];
		if (err) {
			obtenerFiltros(busqueda,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback([], []);
			console.log('Status:', res.statusCode);
		} else {
			var filtrosML = data.filters;
			var filtrosDisponiblesML = data.available_filters;
			
			
			for(var i=0; i<filtrosML.length; i++){
				if(filtrosML[i].id.includes("AMLA")){
					filtros.push({
						nombre : filtrosML[i].name,
						valores : filtrosML[i].values
					});
				}	
			}
			
			for(i=0; i<filtrosDisponiblesML.length; i++){
				if(filtrosDisponiblesML[i].id.includes("AMLA")){
					filtrosDisponibles.push({
						nombre : filtrosDisponiblesML[i].name,
						valores : filtrosDisponiblesML[i].values
					});
				}	
			}

			callback(filtros, filtrosDisponibles);
		}
	});
}

function filterResultsByCategory(searchQuery, jsonProductos, callback){

	//Recorre recursivamente todos los resultados y los agrega según su prediccion de categoria.

	var jsonResults = {
			query: searchQuery,
			categories: [],
			products: [],
			filters: [],
			available_filters:[]
	}; 
	//Funciones que se ejecutaran.
	var evalFunctions = [];

	categoryPredictor.predictQueryCategory(searchQuery, (cbCategories) => {
		try {
			if(cbCategories.length > 0){
				console.log(cbCategories);
				jsonResults.categories = cbCategories;
				obtenerFiltros(searchQuery, (filtros, filtrosDisponibles) => {
					//console.log(JSON.stringify(filtros));
					//console.log(JSON.stringify(filtrosDisponibles));
					jsonResults.filters = filtros;
					jsonResults.available_filters = filtrosDisponibles;
	
					for(var i=0; i<jsonProductos.length; i++){
						var param = {
								filtros: jsonResults.filters,
								filtros_disponibles: jsonResults.available_filters,
								arrayPathBusqueda: cbCategories,
								producto: jsonProductos[i],
								jsonResultados: jsonResults
						};
						evalFunctions.push(evaluateProduct.bind(null, param));
					}
	
					//Realizo todas las funciones de evaluacion en paralelo. Cuando termina, se devuelve.
					async.parallel(evalFunctions, function(err, results) {
						callback(jsonResults);	
					});
				});
			}
			else {
				callback(jsonResults);
				console.log("No se puede predecir la categoria.");
			}
		}
		catch (err){
			callback(jsonResults);
			console.log("No se puede predecir la categoria.");
		}
	});
}

module.exports.filterResultsByCategory = filterResultsByCategory;