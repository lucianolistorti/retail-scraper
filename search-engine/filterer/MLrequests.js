var Producto = require("../../models/producto").Producto;

var request = require('request');

//Returns the json description of searching a query in ML.
function requestSearch(busqueda,callback){
	
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	
	var url = "https://api.mercadolibre.com/sites/MLA/search?q=" + busquedaURL;
	
	request.get({
		url: url,
		timeout: 10000,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestSearch(busqueda,callback);
			console.log('Error:', err);
		} 
		else {
		
			if (res.statusCode !== 200) {
				callback("");
				console.log('Status:', res.statusCode);
			} 
			else {
				callback(data);
			}
		}
		});
}

//Returns the json description of a category obtained by first result of the query in ML Search.
function requestCategoryPredictorSearch(busqueda,callback){

	requestSearch(busqueda, (data) => {
		
		try {
			var primerResult = data.results[0];
			
			var categoria = primerResult.category_id;
			console.log("La categoría es "+ categoria);
			
			var brand = "";
			
			for(var i=0; i<primerResult.attributes.length; i++){
				if(primerResult.attributes[i].id === "BRAND"){
					brand = primerResult.attributes[i].value_name;
				}
			}
			
			requestCategory(categoria, (cbCategory) => {
				var cbData = {
						category: cbCategory,
						brand: brand
				}
				callback(cbData, null);
			});
		}
		catch(err) {
			console.log("No results found for "+ busqueda + ": " + err);
			callback({}, err);
		}
		
	});

}

//Returns the json description of the ML category predictor of a query.
function requestCategoryPredictor(busqueda,callback){
	
	var busquedaURL = busqueda.replace("&","%26").replace("í","%C3%AD").replace("á","a").replace("é","e").replace("ó","o").replace("ú","u");
	
	var url = "https://api.mercadolibre.com/sites/MLA/category_predictor/predict?title=" + busquedaURL;

	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestCategoryPredictor(busqueda,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback("");
			console.log('Status:', res.statusCode);
		} else {
			callback(data);
		}
	});
}

//Returns the json description of the ML category.
function requestCategory(categoria, callback){
	
	var url = "https://api.mercadolibre.com/categories/" + categoria;

	request.get({
		url: url,
		json: true,
		timeout: 20000,
		headers: {'User-Agent': 'request'}
	}, (err, res, data) => {
		if (err) {
			requestCategory(categoria,callback);
			console.log('Error:', err);
		} else if (res.statusCode !== 200) {
			callback("");
			console.log('Status:', res.statusCode);
		} else {
			callback(data);
		}
	});
}

module.exports.requestSearch = requestSearch;
module.exports.requestCategoryPredictorSearch = requestCategoryPredictorSearch;
module.exports.requestCategoryPredictor = requestCategoryPredictor;
