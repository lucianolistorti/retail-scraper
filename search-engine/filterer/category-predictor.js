var mlRequests = require('./MLrequests');

var Producto = require("../../models/producto").Producto;


function predictProductCategory(query, callback){
	
	Producto.findOne({name: query}, (err, product) => {
		//Se realiza el request si no esta en la base de datos.
		if(product){ //esta el producto.
			callback( { categories: product.categories, brand: product.brand }, null);
		}
		else { //no esta el producto.
			mlRequests.requestCategoryPredictorSearch(query, (data, err) => {
				if(!err){
					var categories = data.category.path_from_root;
					if(categories.length > 0){
						var cbData = {
								categories: categories,
								brand: data.brand
						};
						callback(cbData, null);
						guardarProducto(query,cbData);
					}
					else {
						callback({}, "Category could not be found: " + err);
					}
				}
				else {
					callback({}, "Category could not be found: " + err);
				}
				
			});
		}
	});
}

function predictQueryCategory(query, callback){
	mlRequests.requestSearch(query, (data) => {
		var filtersML = data.filters;
		var categories = [];
		
		if(filtersML.length > 0){
			console.log("FILTRO: ") + filtersML;
			for(var i=0; i<filtersML.length; i++){
				if(filtersML[i].id.includes("category")){
					categories = filtersML[i].values[0].path_from_root;
				}	
			}
			callback(categories, null);
			//guardarProducto(query,categories);
		}
		else {
			console.log("sin filtros, entra en category")
			mlRequests.requestCategoryPredictor(query, (data) => {
				categories = data.path_from_root;
				callback(categories, null);
				//guardarProducto(query,categories);
			});
		}	
	});
}

function guardarProducto(busqueda,data){
	console.log("se va a guardar " + busqueda);
	Producto.findOne({ name: busqueda}, (err,producto) => {
		if(!producto){
			var prod = new Producto({
				name: busqueda,
				categories: data.categories,
				brand: data.brand
			});
			prod.save(function(err){
				if(err){
					console.log(String(err));
				}
				else {
					console.log("Datos guardados");
				}
			});
		}
	} );
}

module.exports.predictProductCategory = predictProductCategory;
module.exports.predictQueryCategory = predictQueryCategory;